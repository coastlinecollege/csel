#!/usr/bin/python3
import getpass
import os
import sys
# import pyexcel_ods
import time
import random
import logging
import threading
import shutil
import traceback
import subprocess
from tkinter import *
from tkinter import ttk as ttk
from tkinter import filedialog
from tkinter import messagebox
import tkinter.scrolledtext as ScrolledText
from ttkthemes import ThemedStyle
from subprocess import check_output
import db_handler

Settings = db_handler.Settings()
Categories = db_handler.Categories()
vulnerability_template = {"Disable Guest": {"Definition": 'Enable this to score the competitor for disabling the Guest account in lightdm.',
                                            "Category": 'Account Management'},
                          "Disable Root": {"Definition": 'Enable this to score the competitor for disabling the root login.',
                                           "Category": 'Account Management'},
                          "Critical Users": {"Definition": 'Enable this to penalize the competitor for removing a user.',
                                             "Description": 'This will penalize the competitor for removing a user. To add more users press the "Add" button. To remove a user press the "X" button next to the user you want to remove. Keep it one user per line. To add users that are not on the computer, then you can Category the user name in the field. Otherwise use the drop down to select a user. Do not make the point value negative.',
                                             "Checks": 'User Name:Str',
                                             "Category": 'Account Management'},
                          "Add Admin": {"Definition": 'Enable this to score the competitor for elevating a user to an Administrator.',
                                        "Description": 'This will score the competitor for elevating a user to an Administrator. To add more users press the "Add" button. To remove a user press the "X" button next to the user you want to remove. Keep it one user per line. To add users that are not on the computer, then you can Category the user name in the field. Otherwise use the drop down to select a user.',
                                        "Checks": 'User Name:Str',
                                        "Category": 'Account Management'},
                          "Remove Admin": {"Definition": 'Enable this to score the competitor for demoting a user to Standard user.',
                                           "Description": 'This will score the competitor for demoting a user to Standard user. To add more users press the "Add" button. To remove a user press the "X" button next to the user you want to remove. Keep it one user per line. To add users that are not on the computer, then you can Category the user name in the field. Otherwise use the drop down to select a user.',
                                           "Checks": 'User Name:Str',
                                           "Category": 'Account Management'},
                          "Add User": {"Definition": 'Enable this to score the competitor for adding a user.',
                                       "Description": 'This will score the competitor for adding a user. To add more users press the "Add" button. To remove a user press the "X" button next to the user you want to remove. Keep it one user per line. To add users that are not on the computer, then you can Category the user name in the field. Otherwise use the drop down to select a user.',
                                       "Checks": 'User Name:Str',
                                       "Category": 'Account Management'},
                          "Remove User": {"Definition": 'Enable this to score the competitor for removing a user.',
                                          "Description": 'This will score the competitor for removing a user. To add more users press the "Add" button. To remove a user press the "X" button next to the user you want to remove. Keep it one user per line. To add users that are not on the computer, then you can Category the user name in the field. Otherwise use the drop down to select a user.',
                                          "Checks": 'User Name:Str',
                                          "Category": 'Account Management'},
                          "User Change Password": {"Definition": 'Enable this to score the competitor for changing a users password.',
                                                   "Description": 'This will score the competitor for changing a users password. To add more users press the "Add" button. To remove a user press the "X" button next to the user you want to remove. Keep it one user per line. To add users that are not on the computer, then you can Category the user name in the field. Otherwise use the drop down to select a user.',
                                                   "Checks": 'User Name:Str',
                                                   "Category": 'Account Management'},
                          "Unlock User": {"Definition": 'Enable this to score the competitor for unlocking a users account.',
                                          "Description": 'This will score the competitor for unlocking a users account. To add more users press the "Add" button. To remove a user press the "X" button next to the user you want to remove. Keep it one user per line. To add users that are not on the computer, then you can Category the user name in the field. Otherwise use the drop down to select a user.',
                                          "Checks": 'User Name:Str',
                                          "Category": 'Account Management'},
                          "Lock User": {"Definition": 'Enable this to score the competitor for locking a users account.',
                                        "Description": 'This will score the competitor for locking a users account. To add more users press the "Add" button. To remove a user press the "X" button next to the user you want to remove. Keep it one user per line. To add users that are not on the computer, then you can Category the user name in the field. Otherwise use the drop down to select a user.',
                                        "Checks": 'User Name:Str',
                                        "Category": 'Account Management'},
                          "Add User to Group": {"Definition": 'Enable this to score the competitor for adding a user to a group other than the Administrative group.',
                                                "Description": 'This will score the competitor for adding a user to a group other than the Administrative group. To add more users press the "Add" button. To remove a user press the "X" button next to the user you want to remove. Keep it one user  and group per line. To add users or group that are not on the computer, then you can type the user or group name in the field. Otherwise use the drop down to select a user or group.',
                                                "Checks": 'User Name:Str,Group Name:Str',
                                                "Category": 'Account Management'},
                          "Remove User from Group": {"Definition": 'Enable this to score the competitor for removing a user from a group other than the Administrative group.',
                                                     "Description": 'This will score the competitor for removing a user from a group other than the Administrative group. To add more users press the "Add" button. To remove a user press the "X" button next to the user you want to remove. Keep it one user and group per line. To add users or group that are not on the computer, then you can type the user or group name in the field. Otherwise use the drop down to select a user or group.',
                                                     "Checks": 'User Name:Str,Group Name:Str',
                                                     "Category": 'Account Management'},
                          "Secure Sudoers": {"Definition": 'Enable this to score the competitor for removing user names from the /etc/sudoers file.',
                                             "Description": 'Enable this to score the competitor for removing user names from the /etc/sudoers file. To add more users press the "Add" button. To remove a user press the "X" button next to the user you want to remove.',
                                             "Checks": 'User Name:Str',
                                             "Category": 'Local Policy'},
                          "Disable Auto Login": {"Definition": 'Remove all users from auto login. (lightdm)',
                                                 "Category": 'Local Policy'},
                          "Disable User Greeter": {"Definition": 'Enable this to score the competitor for disabling the user greeter in lightdm.',
                                                   "Category": 'Local Policy'},
                          "Enable ssh root login": {"Definition": 'Enable this to score the competitor for enabling root login using ssh.(Rare Case)',
                                                    "Category": 'Local Policy'},
                          "Disable ssh root login": {"Definition": 'Enable this to score the competitor for disabling root login using ssh.',
                                                     "Category": 'Local Policy'},
                          "Turn On Firewall": {"Definition": 'Enable this to score the competitor for turning on the firewall. (ufw)',
                                               "Category": 'Local Policy'},
                          "Firewall Rules": {"Definition": 'List of rules that need to be added or removed.',
                                             "Description": 'This will score the competitor for adding, removing, or modifying the desired rule in the firewall. To add more rules press the "Add" button. To remove a rule press the "X" button next to the rule you want to remove. Keep it one port per line and the ports must be numbers. The traffic is the application or protocol that will be allowed through the designated port. Uses ufw rules.',
                                             "Checks": 'Modification:Str,Firewall Rule:Str,Port Number:Int,Traffic:Str',
                                             "Category": 'Local Policy'},
                          "Monitor Ports": {"Definition": 'List of ports that need to be open or closed.',
                                            "Description": 'This will score the competitor for changing the desired port to the desired state. To add more ports press the "Add" button. To remove a port press the "X" button next to the port you want to remove. Keep it one port per line and the ports must be numbers.',
                                            "Category": 'Local Policy'},
                          "Minimum Password Age": {"Definition": 'Enable this to score the competitor for setting the minimum password age between 30 and 60. (login.defs)',
                                                   "Category": 'Local Policy'},
                          "Maximum Password Age": {"Definition": 'Enable this to score the competitor for setting the maximum password age between 60 and 90. (login.defs)',
                                                   "Category": 'Local Policy'},
                          "Minimum Password Length": {"Definition": 'Enable this to score the competitor for setting the minimum password length between 10 and 20. (pam.d/common-password)',
                                                      "Category": 'Local Policy'},
                          "Maximum Login Tries": {"Definition": 'Enable this to score the competitor for setting the maximum login tries between 5 and 10. (login.defs)',
                                                  "Category": 'Local Policy'},
                          "Password History": {"Definition": 'Enable this to score the competitor for setting the password history between 5 and 10. (pam.d/common-password)',
                                               "Category": 'Local Policy'},
                          "Password Complexity": {"Definition": 'Enable this to score the competitor for enabling password complexity. (pam.d/common-password)',
                                                  "Category": 'Local Policy'},
                          "Critical Programs": {"Definition": 'Enable this to penalize the competitor for removing a program.',
                                                "Description": 'This will penalize the competitor for removing a program. To add more programs press the "Add" button. To remove a program press the "X" button next to the program you want to remove. Keep it one program per line.',
                                                "Checks": 'Program Name:Str',
                                                "Category": 'Program Management'},
                          "Good Program": {"Definition": 'Enable this to score the competitor for installing a program.',
                                           "Description": 'This will score the competitor for installing a program. To add more programs press the "Add" button. To remove a program press the "X" button next to the program you want to remove. Keep it one program per line.',
                                           "Checks": 'Program Name:Str',
                                           "Category": 'Program Management'},
                          "Bad Program": {"Definition": 'Enable this to score the competitor for uninstalling a program.',
                                          "Description": 'This will score the competitor for uninstalling a program. To add more programs press the "Add" button. To remove a program press the "X" button next to the program you want to remove. Keep it one program per line.',
                                          "Checks": 'Program Name:Str',
                                          "Category": 'Program Management'},
                          "Update Program": {"Definition": 'Enable this to score the competitor for updating a program.',
                                             "Description": 'This will score the competitor for updating a program. To add more programs press the "Add" button. To remove a program press the "X" button next to the program you want to remove. Keep it one program per line.',
                                             "Checks": 'Program Name:Str',
                                             "Category": 'Program Management'},
                          "Critical Services": {"Definition": 'Enable this to penalize the competitor for modifying a services run ability.',
                                                "Description": 'This will penalize the competitor for modifying a services run ability. To add more services press the "Add" button. To remove a service press the "X" button next to the service you want to remove. Keep it one service per line.',
                                                "Checks": 'Service Name:Str,Service State:Str,Service Start Mode:Str',
                                                "Category": 'Program Management'},
                          "Services": {"Definition": 'Enable this to score the competitor for modifying a services run ability.',
                                       "Description": 'This will score the competitor for modifying a services run ability. To add more services press the "Add" button. To remove a service press the "X" button next to the service you want to remove. Keep it one service per line.',
                                       "Checks": 'Service Name:Str,Service State:Str,Service Start Mode:Str',
                                       "Category": 'Program Management'},
                          "Forensic": {"Definition": 'Enable this to score the competitor for answering forensic a question.',
                                       "Description": 'This will score the competitor for answering forensic questions. To add more questions press the "Add" button. To remove questions press the "X" button next to the question you want to remove. The location will automatically be set to the desktop of that is set in the main menu.',
                                       "Checks": 'Question:Str,Answers:Str,Location:Str',
                                       "Category": 'File Management'},
                          "Bad File": {"Definition": 'Enable this to score the competitor for deleting a file.',
                                       "Description": 'This will score the competitor for deleting a file. To add more files press the "Add" button. To remove a file press the "X" button next to the file you want to remove. Keep it one file per line.',
                                       "Checks": 'File Path:Str',
                                       "Category": 'File Management'},
                          "Check Hosts": {"Definition": 'Enable this to score the competitor for clearing the hosts file.',
                                          "Description": 'This will score the competitor for clearing the hosts file. To add more items to remove press the "Add" button. To remove an item press the "X" button next to the file you want to remove. Keep it one item per line.',
                                          "Checks": 'Text:Str',
                                          "Category": 'File Management'},
                          "Add Text to File": {"Definition": 'Enable this to score the competitor for adding text to a file.',
                                               "Description": 'This will score the competitor for adding text to a file. To add more files press the "Add" button. To remove a file press the "X" button next to the file you want to remove. Keep it one file per line.',
                                               "Checks": 'Text to Add:Str,File Path:Str',
                                               "Category": 'File Management'},
                          "Remove Text From File": {"Definition": 'Enable this to score the competitor for removing text from a file.',
                                                    "Description": 'This will score the competitor for removing text from a file. To add more files press the "Add" button. To remove a file press the "X" button next to the file you want to remove. Keep it one file per line.',
                                                    "Checks": 'Text to Remove:Str,File Path:Str',
                                                    "Category": 'File Management'},
                          "File Permissions": {"Definition": 'Enable this to score the competitor for changing the permissions on a file.',
                                               "Description": 'This will score the competitor for changing the permissions on a file based on category. To add more files press the "Add" button. To remove a file press the "X" button next to the file you want to remove. Keep it one file per line. The current file permissions will automatically be set when the file is selected. If you want to score based on different settings than change them accordingly after file selection.',
                                               "Checks": 'Permission to Set:Int,File Path:Str',
                                               "Category": 'File Management'},
                          "File Share": {"Definition": '(WIP)Enable this to score the competitor for changing the share permissions on a file.',
                                         "Description": '(WIP)This will score the competitor for changing the share permissions on a file based on category. To add more files press the "Add" button. To remove a file press the "X" button next to the file you want to remove. Keep it one file per line. The current file permissions will automatically be set when the file is selected. If you want to score based on different settings than change them accordingly after file selection.',
                                         "Checks": 'Share Permissions:Str,File Path:Str',
                                         "Category": 'File Management'},
                          "Anti-Virus": {"Definition": 'Enable this to score the competitor for installing an anti-virus.',
                                         "Category": 'Miscellaneous'},
                          "Update Check Period": {"Definition": 'Enable this to score the competitor for setting the period the system checks for updates to once a week.',
                                                  "Category": 'Miscellaneous'},
                          "Update Auto Install": {"Definition": 'Enable this to score the competitor for setting the system updates to automatically install updates.',
                                                  "Category": 'Miscellaneous'},
                          "Disable IPv6 Login": {"Definition": '(WIP)Enable this to score the competitor for setting net.ipv6.conf.all.disable_ipv6 to 1.',
                                                 "Category": 'Miscellaneous'},
                          "Update Kernel": {"Definition": '(WIP)Enable this to score the competitor for Update the kernel to the newest version.',
                                            "Category": 'Miscellaneous'},
                          "Cron Tab": {"Definition": 'This will score the competitor for removing a task from the cron tab.',
                                       "Description": 'This will score the competitor for removing a task from the cron tab. To add more tasks press the "Add" button. To remove a task press the "X" button next to the task you want to remove. Keep it one task per line.',
                                       "Checks": 'User Name:Str,Task Name:Str',
                                       "Category": 'Miscellaneous'},
                          "Check Startup": {"Definition": '(WIP)Enable this to score the competitor for removing or disabling a program from the startup.',
                                            "Description": 'This will score the competitor for removing or disabling a program from the startup. To add more programs press the "Add" button. To remove a program press the "X" button next to the program you want to remove. Keep it one program per line.',
                                            "Checks": 'Program Name:Str',
                                            "Category": 'Miscellaneous'},
                          }
Vulnerabilities = db_handler.OptionTables(vulnerability_template)
Vulnerabilities.initialize_option_table()
vuln_settings = {}


class VerticalScrolledFrame(Frame):
    """A pure Tkinter scrollable frame that actually works!
    * Use the 'interior' attribute to place widgets inside the scrollable frame
    * Construct and pack/place/grid normally
    * This frame only allows vertical scrolling
    """

    def __init__(self, parent, *args, **kw):
        Frame.__init__(self, parent, *args, **kw)
        # create a canvas object and a vertical scrollbar for scrolling it
        self.canvas = canvas = Canvas(self, bd=0, highlightthickness=0)
        vscrollbar = ttk.Scrollbar(self, orient=VERTICAL, command=canvas.yview)
        vscrollbar.pack(fill=Y, side=RIGHT, expand=FALSE)
        canvas.pack(side=LEFT, fill=BOTH, expand=TRUE)
        vscrollbar.config(command=canvas.yview)
        # reset the view
        canvas.xview_moveto(0)
        canvas.yview_moveto(0)
        # create a frame inside the canvas which will be scrolled with it
        self.interior = interior = ttk.Frame(canvas)
        interior_id = canvas.create_window(0, 0, window=interior, anchor=NW)

        # track changes to the canvas and frame width and sync them,
        # also updating the scrollbar

        def _configure_interior(event):
            # update the scrollbars to match the size of the inner frame
            size = (interior.winfo_reqwidth(), interior.winfo_reqheight())
            canvas.config(scrollregion="0 0 %s %s" % size)
            if interior.winfo_reqwidth() != canvas.winfo_width():
                # update the canvas's width to fit the inner frame
                canvas.config(width=interior.winfo_reqwidth())

        interior.bind('<Configure>', _configure_interior)

        def _configure_canvas(event):
            if interior.winfo_reqwidth() != canvas.winfo_width():
                # update the inner frame's width to fill the canvas
                canvas.itemconfigure(interior_id, width=canvas.winfo_width())
                canvas.configure(background=root.ttkStyle.lookup(".", "background"), yscrollcommand=vscrollbar.set)

        canvas.bind('<Configure>', _configure_canvas)


class TextHandler(logging.Handler):
    """https://stackoverflow.com/questions/13318742/python-logging-to-tkinter-text-widget"""
    # This class allows you to log to a Tkinter Text or ScrolledText widget
    # Adapted from Moshe Kaplan: https://gist.github.com/moshekaplan/c425f861de7bbf28ef06

    def __init__(self, text):
        # run the regular Handler __init__
        logging.Handler.__init__(self)
        # Store a reference to the Text it will log to
        self.text = text

    def emit(self, record):
        msg = self.format(record)

        def append():
            self.text.configure(state='normal')
            self.text.insert(END, msg)
            self.text.configure(state='disabled')
            # Autoscroll to the bottom
            self.text.yview(END)
        # This is necessary because we can't modify the Text from other threads
        self.text.after(0, append)


class SetupWindow(Frame):
    # This class defines the graphical user interface

    def __init__(self, parent, *args, **kwargs):
        Frame.__init__(self, parent, *args, **kwargs)
        self.progress = ttk.Progressbar(self, orient="horizontal", length=200, mode="determinate")
        self.exit = ttk.Button(self, text="Finish", command=lambda: (self.root.destroy(), sys.exit()), state='disabled')
        self.root = parent
        self.build_gui()

    def build_gui(self):
        # Build GUI
        self.root.title("Engine Setup")
        self.root.geometry('+{0}+{1}'.format(int(root.winfo_screenwidth() * 9 / 24), int(root.winfo_screenheight() * 3 / 8)))
        set_default_themes(self.root)
        self.pack(fill=BOTH, expand=TRUE)

        # Add text widget to display logging info
        st = ScrolledText.ScrolledText(self, height=8, state='disabled')
        st.configure(font='TkFixedFont')
        st.pack(fill=X)

        # Add a progressbar to display progress
        self.progress.pack(fill=X)
        self.progress["value"] = 0
        self.progress["maximum"] = 100

        self.exit.pack(anchor=SE, fill=X)

        # Create textLogger
        text_handler = TextHandler(st)

        # Logging configuration
        logging.basicConfig(filename='test.log', level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')

        # Add the handler to logger
        logger = logging.getLogger()
        logger.addHandler(text_handler)


class Config(Tk):
    def __init__(self, *args, **kwargs):
        Tk.__init__(self, *args, **kwargs)

        nb = ttk.Notebook(self)
        MainPage = ttk.Frame(nb)

        self.MenuSettings = Settings.get_settings()
        temp_style = self.MenuSettings["Style"].get()

        ttk.Button(MainPage, text='Save', command=lambda: (save_config())).grid(sticky=EW)
        ttk.Label(MainPage, text="Leave blank if the current logged in users is the main otherwise enter the path manually.").grid(row=0, column=1, sticky=W, columnspan=4)
        ttk.OptionMenu(MainPage, self.MenuSettings["Style"], *themeList).grid(row=0, column=5, sticky=EW)
        self.MenuSettings["Style"].set(temp_style)
        ttk.Button(MainPage, text='Set', width=5, command=lambda: change_theme).grid(row=0, column=6)
        ttk.Button(MainPage, text='Commit', command=lambda: (commit_config())).grid(row=1, sticky=EW)
        ttk.Entry(MainPage, textvariable=self.MenuSettings["Desktop"]).grid(row=1, column=1, columnspan=4, sticky=EW)
        ttk.Checkbutton(MainPage, text='Silent Miss', variable=self.MenuSettings["Silent Mode"]).grid(row=2, sticky=W)
        ttk.Label(MainPage, text='Check this box to hide missed items (Similar to competition)').grid(row=2, column=1, columnspan=5, sticky=W)
        ttk.Checkbutton(MainPage, text='Server Mode', variable=self.MenuSettings["Server Mode"], command=lambda: (serverL.configure(state='enable'), serverE.configure(state='enable'), userL.configure(state='enable'), userE.configure(state='enable'), passL.configure(state='enable'), passE.configure(state='enable'))).grid(row=3, sticky=W)
        ttk.Label(MainPage, text='Check this box to enable an FTP server to save the scores (Similar to competition)').grid(row=3, column=1, columnspan=5, sticky=W)
        serverL = ttk.Label(MainPage, text='Server Name/IP', state='disable')
        serverL.grid(row=4, sticky=E)
        serverE = ttk.Entry(MainPage, textvariable=self.MenuSettings["Server Name"], state='disable', width=30)
        serverE.grid(row=4, column=1, sticky=EW)
        userL = ttk.Label(MainPage, text='User Name', state='disable')
        userL.grid(row=4, column=2, sticky=E)
        userE = ttk.Entry(MainPage, textvariable=self.MenuSettings["Server User"], state='disable', width=30)
        userE.grid(row=4, column=3, sticky=EW)
        passL = ttk.Label(MainPage, text='Password', state='disable')
        passL.grid(row=4, column=4, sticky=E)
        passE = ttk.Entry(MainPage, textvariable=self.MenuSettings["Server Password"], state='disable', width=30)
        passE.grid(row=4, column=5, sticky=EW)
        ttk.Label(MainPage, text="Total Points:").grid(row=5, column=0)
        ttk.Label(MainPage, textvariable=self.MenuSettings["Tally Points"], font='Verdana 10 bold', wraplength=150).grid(row=5, column=1)
        ttk.Label(MainPage, text="Total Vulnerabilities:").grid(row=6, column=0)
        ttk.Label(MainPage, textvariable=self.MenuSettings["Tally Vulnerabilities"], font='Verdana 10 bold', wraplength=150).grid(row=6, column=1)

        pages = {}
        for category in Categories.get_categories():
            page = VerticalScrolledFrame(nb)
            pageList = ttk.Frame(page.interior)
            pageList.pack(fill=X)
            pageList.grid_columnconfigure(1, weight=1)
            pageIn = ttk.Frame(page)
            pageIn.pack(before=page.canvas, fill=X)
            pageIn.grid_columnconfigure(1, weight=1)
            ttk.Label(pageIn, text=category.description, padding='10 5').grid(row=0, column=0, columnspan=3)
            ttk.Label(pageIn, text='Vulnerabilities', font='Verdana 12 bold').grid(row=1, column=0, stick=W)
            ttk.Label(pageIn, text="Points", font='Verdana 12 bold').grid(row=1, column=2)
            for i, vuln in enumerate(Vulnerabilities.get_option_template_by_category(category.id)):
                vuln_settings.update({vuln.name: {}})
                vuln_settings[vuln.name] = Vulnerabilities.get_option_table(vuln.name).copy()
                self.add_option(pageList, vuln_settings[vuln.name], vuln.name, i * 2 + 2, nb)
            pages.update({category.name: page})

        ReportPage = VerticalScrolledFrame(nb)
        ReportPageList = ttk.Frame(ReportPage.interior)
        ReportPageList.pack(fill=X)
        ReportPageIn = ttk.Frame(ReportPage)
        ReportPageIn.pack(before=ReportPage.canvas, fill=X)
        ttk.Button(ReportPageIn, text='Export to csv'''', command=generate_CSV_export''').grid(row=0, column=0, stick=EW)
        ttk.Button(ReportPageIn, text='Export to HTML', command=generate_HTML_export).grid(row=1, column=0, stick=EW)
        ttk.Button(ReportPageIn, text='Generate', command=lambda: (self.generate_report(ReportPageList))).grid(row=2, column=0, stick=EW)
        ttk.Label(ReportPageIn, text='This section is for reviewing the options that will be scored. To view the report press the "Generate" button. To export this report to a .csv file press the "Export to CSV" button(WIP). To export this report to a web page press the "Export to HTML" button.').grid(row=0, column=1, rowspan=3, columnspan=4)
        ttk.Separator(ReportPageIn, orient=HORIZONTAL).grid(row=3, column=0, columnspan=5, sticky=EW)

        nb.add(MainPage, text='Main Page')
        for page in pages:
            nb.add(pages[page], text=page)
        nb.add(ReportPage, text='Report')

        nb.pack(expand=1, fill="both")

    def add_option(self, frame, entry, name, row, return_frame):
        ttk.Checkbutton(frame, text=name, variable=entry[1]["Enabled"]).grid(row=row, column=0, stick=W)
        ttk.Label(frame, text=Vulnerabilities.get_option_template(name).definition).grid(row=row, column=1, stick=W)
        if len(entry[1]["Checks"]) > 0:
            ttk.Button(frame, text='Modify', command=lambda: self.modify_settings(name, entry, return_frame)).grid(row=row, column=2)
        else:
            Entry(frame, width=5, textvariable=entry[1]["Points"], font='Verdana 10').grid(row=row, column=2)
        ttk.Separator(frame, orient=HORIZONTAL).grid(row=row + 1, column=0, columnspan=3, sticky=EW)

    def modify_settings(self, name, entry, packing):
        self.pack_slaves()[0].pack_forget()
        modifyPage = VerticalScrolledFrame(self)
        modifyPage.pack(expand=1, fill=BOTH)
        modifyPageList = modifyPage.interior
        modifyPageList.pack(fill=X)
        modifyPageIn = ttk.Frame(modifyPage)
        modifyPageIn.pack(before=modifyPage.canvas, fill=X)
        if entry[1]["Enabled"].get() != 1:
            entry[1]["Enabled"].set(1)
        ttk.Button(modifyPageIn, text="Save", command=lambda: (self.pack_slaves()[0].pack_forget(), packing.pack(expand=1, fill="both"), Vulnerabilities.update_table(name, entry))).grid(row=0, column=0, sticky=EW)
        ttk.Label(modifyPageIn, text=name + ' Modification', font='Verdana 15').grid(row=0, column=1, columnspan=len(entry[1]["Checks"]))
        ttk.Button(modifyPageIn, text="Add", command=lambda: (add_row(modifyPageList, entry, name))).grid(row=1, column=0, sticky=EW)
        ttk.Label(modifyPageIn, text=Vulnerabilities.get_option_template(name).description, wraplength=int(self.winfo_screenwidth() * 2 / 3 - 100)).grid(row=1, column=1, columnspan=len(entry[1]["Checks"]))
        ttk.Label(modifyPageIn, text="Points", font='Verdana 10 bold', width=10).grid(row=2, column=0)
        r = 1
        for i, t in enumerate(entry[1]["Checks"]):
            if "Permission" not in t:
                modifyPageIn.grid_columnconfigure(i + 1, weight=1)
            ttk.Label(modifyPageIn, text=t, font='Verdana 10 bold').grid(row=2, column=i + 1)
            r = i + 2
        ttk.Label(modifyPageIn, text="Remove", font='Verdana 10 bold').grid(row=2, column=r)
        for vuln in entry:
            if vuln != 1:
                load_modify_settings(modifyPageList, entry, name, vuln)

    def generate_report(self, frame):
        save_config()
        for i in frame.grid_slaves():
            i.destroy()
        wrap = int(self.winfo_screenwidth() * 2 / 3 / 5) - 86
        final_row = 5

        frame.rowconfigure(4, weight=1)
        report_frame = ttk.Frame(frame)
        report_frame.grid(row=4, column=0, columnspan=5, sticky=NSEW)
        categories = Categories.get_categories()
        for cat_row, category in enumerate(categories):
            category_frame = ttk.Frame(report_frame, borderwidth=1, relief=GROOVE)
            category_frame.grid(row=cat_row, column=1, sticky=NSEW)
            ttk.Label(category_frame, text=category.name).grid(row=0, column=0)
            vulns_row = ttk.Frame(category_frame, borderwidth=1, relief=GROOVE)
            vulns_row.grid(row=0, column=1, sticky=EW)
            vulnerabilities = Vulnerabilities.get_option_template_by_category(category.id)
            cat_tested = False
            for vuln_row, vulnerability in enumerate(vulnerabilities):
                settings = Vulnerabilities.get_option_table(vulnerability.name)
                if int(settings[1]["Enabled"].get()) == 1:
                    vulnerability_frame = ttk.Frame(vulns_row, borderwidth=1, relief=GROOVE)
                    vulnerability_frame.grid(row=vuln_row, column=1, sticky=EW)
                    ttk.Label(vulnerability_frame, text=vulnerability.name).grid(row=0, column=0)
                    setting_frame = ttk.Frame(vulnerability_frame, borderwidth=1, relief=GROOVE, padding=1)
                    setting_frame.grid(row=0, column=1, sticky=EW)
                    cat_tested = True
                    width = len(settings[1]["Checks"]) + 1
                    temp_col = 1
                    ttk.Label(setting_frame, text="Points").grid(row=0, column=0)
                    for check in settings[1]["Checks"]:
                        ttk.Label(setting_frame, text=check).grid(row=0, column=temp_col)
                        temp_col += 1
                    final_row += 1
                    for set_row, setting in enumerate(settings):
                        if (width > 0 and setting != 1) or (width == 1):
                            ttk.Separator(setting_frame, orient=HORIZONTAL).grid(row=set_row * 2 + 1, column=0, columnspan=5, sticky=EW)
                            temp_col = 1
                            ttk.Label(setting_frame, text=settings[setting]["Points"].get()).grid(row=set_row * 2 + 2, column=0)
                            for check in settings[setting]["Checks"]:
                                if check == "Permission to Set":
                                    binary = [IntVar(), IntVar(), IntVar(), IntVar(), IntVar(), IntVar(), IntVar(), IntVar(), IntVar()]
                                    perm = ttk.Frame(setting_frame)
                                    perm.grid(row=set_row * 2 + 2, column=temp_col, sticky=EW)
                                    ttk.Label(perm, text="Owner").grid(row=0, column=0)
                                    ttk.Checkbutton(perm, text="Read", variable=binary[0], state='disable').grid(row=1, column=0, sticky=W)
                                    ttk.Checkbutton(perm, text="Write", variable=binary[1], state='disable').grid(row=2, column=0, sticky=W)
                                    ttk.Checkbutton(perm, text="Execute", variable=binary[2], state='disable').grid(row=3, column=0, sticky=W)
                                    ttk.Label(perm, text="Group").grid(row=0, column=1)
                                    ttk.Checkbutton(perm, text="Read", variable=binary[3], state='disable').grid(row=1, column=1, sticky=W)
                                    ttk.Checkbutton(perm, text="Write", variable=binary[4], state='disable').grid(row=2, column=1, sticky=W)
                                    ttk.Checkbutton(perm, text="Execute", variable=binary[5], state='disable').grid(row=3, column=1, sticky=W)
                                    ttk.Label(perm, text="All").grid(row=0, column=2)
                                    ttk.Checkbutton(perm, text="Read", variable=binary[6], state='disable').grid(row=1, column=2, sticky=W)
                                    ttk.Checkbutton(perm, text="Write", variable=binary[7], state='disable').grid(row=2, column=2, sticky=W)
                                    ttk.Checkbutton(perm, text="Execute", variable=binary[8], state='disable').grid(row=3, column=2, sticky=W)
                                    permissions = int(settings[setting]["Checks"]["Permission to Set"].get())
                                    for exp in range(0, 9, 1):
                                        bit = pow(2, 8 - exp)
                                        if permissions >= bit:
                                            binary[exp].set(1)
                                            permissions -= bit
                                        elif permissions == 0:
                                            break
                                else:
                                    ttk.Label(setting_frame, text=settings[setting]["Checks"][check].get()).grid(row=set_row * 2 + 2, column=temp_col)
                                temp_col += 1
            if not cat_tested:
                category_frame.destroy()


def load_modify_settings(frame, entry, name, idx):
    modifyPageListRow = ttk.Frame(frame)
    modifyPageListRow.pack(fill=X)
    ttk.Entry(modifyPageListRow, width=10, textvariable=entry[idx]["Points"]).grid(row=0, column=0)
    c = 0
    binary = [IntVar(), IntVar(), IntVar(), IntVar(), IntVar(), IntVar(), IntVar(), IntVar(), IntVar()]
    for r, t in enumerate(entry[idx]["Checks"]):
        r += 1
        if t == "File Path":
            modifyPageListRow.grid_columnconfigure(r, weight=1)
            path = ttk.Frame(modifyPageListRow)
            path.grid(row=0, column=r, sticky=EW)
            path.grid_columnconfigure(0, weight=1)
            ttk.Label(path, text="To point to a directory check directory otherwise leave unchecked.").grid(row=1, column=0, sticky=E)
            switch = IntVar()
            ttk.Checkbutton(path, variable=switch, text="Directory").grid(row=1, column=1)
            ttk.Entry(path, textvariable=entry[idx]["Checks"][t]).grid(row=0, column=0, sticky=EW)
            ttk.Button(path, text='...', command=lambda: set_file_or_directory(entry[idx]["Checks"], switch, name, binary)).grid(row=0, column=1)
            c = r + 1
        elif t == "Permission to Set":
            perm = ttk.Frame(modifyPageListRow)
            perm.grid(row=0, column=r, sticky=EW)
            ttk.Label(perm, text="Owner").grid(row=0, column=0)
            ttk.Checkbutton(perm, text="Read", variable=binary[0], command=lambda: (permission_modification(entry[idx]["Checks"]["Permission to Set"], 256, binary[0]))).grid(row=1, column=0, sticky=W)
            ttk.Checkbutton(perm, text="Write", variable=binary[1], command=lambda: (permission_modification(entry[idx]["Checks"]["Permission to Set"], 128, binary[1]))).grid(row=2, column=0, sticky=W)
            ttk.Checkbutton(perm, text="Execute", variable=binary[2], command=lambda: (permission_modification(entry[idx]["Checks"]["Permission to Set"], 64, binary[2]))).grid(row=3, column=0, sticky=W)
            ttk.Label(perm, text="Group").grid(row=0, column=1)
            ttk.Checkbutton(perm, text="Read", variable=binary[3], command=lambda: (permission_modification(entry[idx]["Checks"]["Permission to Set"], 32, binary[3]))).grid(row=1, column=1, sticky=W)
            ttk.Checkbutton(perm, text="Write", variable=binary[4], command=lambda: (permission_modification(entry[idx]["Checks"]["Permission to Set"], 16, binary[4]))).grid(row=2, column=1, sticky=W)
            ttk.Checkbutton(perm, text="Execute", variable=binary[5], command=lambda: (permission_modification(entry[idx]["Checks"]["Permission to Set"], 8, binary[5]))).grid(row=3, column=1, sticky=W)
            ttk.Label(perm, text="All").grid(row=0, column=2)
            ttk.Checkbutton(perm, text="Read", variable=binary[6], command=lambda: (permission_modification(entry[idx]["Checks"]["Permission to Set"], 4, binary[6]))).grid(row=1, column=2, sticky=W)
            ttk.Checkbutton(perm, text="Write", variable=binary[7], command=lambda: (permission_modification(entry[idx]["Checks"]["Permission to Set"], 2, binary[7]))).grid(row=2, column=2, sticky=W)
            ttk.Checkbutton(perm, text="Execute", variable=binary[8], command=lambda: (permission_modification(entry[idx]["Checks"]["Permission to Set"], 1, binary[8]))).grid(row=3, column=2, sticky=W)
            permissions = int(entry[idx]["Checks"]["Permission to Set"].get())
            for exp in range(0, 9, 1):
                bit = pow(2, 8 - exp)
                if permissions >= bit:
                    binary[exp].set(1)
                    permissions -= bit
                elif permissions == 0:
                    break
            c = r + 1
        elif t == "Service State":
            modifyPageListRow.grid_columnconfigure(r, weight=1)
            ttk.OptionMenu(modifyPageListRow, entry[idx]["Checks"][t], *["Running", "Running", "Stopped"]).grid(row=0, column=r, sticky=EW)
            c = r + 1
        elif t == "Service Start Mode":
            modifyPageListRow.grid_columnconfigure(r, weight=1)
            ttk.OptionMenu(modifyPageListRow, entry[idx]["Checks"][t], *["Auto", "Auto", "Manual", "Disabled"]).grid(row=0, column=r, sticky=EW)
            c = r + 1
        elif t == "User Name":
            modifyPageListRow.grid_columnconfigure(r, weight=1)
            user_list = get_user_list()
            ttk.Combobox(modifyPageListRow, textvariable=entry[idx]["Checks"][t], values=user_list).grid(row=0, column=r, sticky=EW)
            c = r + 1
        elif t == "Group Name":
            modifyPageListRow.grid_columnconfigure(r, weight=1)
            group_list = get_group_list()
            ttk.Combobox(modifyPageListRow, textvariable=entry[idx]["Checks"][t], values=group_list).grid(row=0, column=r, sticky=EW)
            c = r + 1
        else:
            modifyPageListRow.grid_columnconfigure(r, weight=1)
            ttk.Entry(modifyPageListRow, textvariable=entry[idx]["Checks"][t]).grid(row=0, column=r, sticky=EW)
            c = r + 1
    ttk.Button(modifyPageListRow, text='X', width=8, command=lambda: (remove_row(entry, idx, modifyPageListRow), Vulnerabilities.remove_from_table(name, idx))).grid(row=0, column=c, sticky=W)


def add_row(frame, entry, name):
    idx = Vulnerabilities.add_to_table(name).id
    entry.update({idx: Vulnerabilities.get_option_table(name)[idx]})

    mod_frame = ttk.Frame(frame)
    mod_frame.pack(fill=X)

    ttk.Entry(mod_frame, width=10, textvariable=entry[idx]["Points"]).grid(row=0, column=0)
    c = 0
    binary = [IntVar(), IntVar(), IntVar(), IntVar(), IntVar(), IntVar(), IntVar(), IntVar(), IntVar()]
    for r, t in enumerate(entry[idx]["Checks"]):
        r += 1
        if t == "File Path":
            mod_frame.grid_columnconfigure(r, weight=1)
            path = ttk.Frame(mod_frame)
            path.grid(row=0, column=r, sticky=EW)
            path.grid_columnconfigure(0, weight=1)
            ttk.Label(path, text="To point to a directory check directory otherwise leave unchecked.").grid(row=1, column=0, sticky=E)
            switch = IntVar()
            ttk.Checkbutton(path, variable=switch, text="Directory").grid(row=1, column=1)
            ttk.Entry(path, textvariable=entry[idx]["Checks"][t]).grid(row=0, column=0, sticky=EW)
            ttk.Button(path, text='...', command=lambda: set_file_or_directory(entry[idx]["Checks"], switch, name, binary)).grid(row=0, column=1)
            c = r + 1
        elif t == "Permission to Set":
            perm = ttk.Frame(mod_frame)
            perm.grid(row=0, column=r, sticky=EW)
            ttk.Label(perm, text="Owner").grid(row=0, column=0)
            ttk.Checkbutton(perm, text="Read", variable=binary[0], command=lambda: (permission_modification(entry[idx]["Checks"]["Permission to Set"], 256, binary[0]))).grid(row=1, column=0, sticky=W)
            ttk.Checkbutton(perm, text="Write", variable=binary[1], command=lambda: (permission_modification(entry[idx]["Checks"]["Permission to Set"], 128, binary[1]))).grid(row=2, column=0, sticky=W)
            ttk.Checkbutton(perm, text="Execute", variable=binary[2], command=lambda: (permission_modification(entry[idx]["Checks"]["Permission to Set"], 64, binary[2]))).grid(row=3, column=0, sticky=W)
            ttk.Label(perm, text="Group").grid(row=0, column=1)
            ttk.Checkbutton(perm, text="Read", variable=binary[3], command=lambda: (permission_modification(entry[idx]["Checks"]["Permission to Set"], 32, binary[3]))).grid(row=1, column=1, sticky=W)
            ttk.Checkbutton(perm, text="Write", variable=binary[4], command=lambda: (permission_modification(entry[idx]["Checks"]["Permission to Set"], 16, binary[4]))).grid(row=2, column=1, sticky=W)
            ttk.Checkbutton(perm, text="Execute", variable=binary[5], command=lambda: (permission_modification(entry[idx]["Checks"]["Permission to Set"], 8, binary[5]))).grid(row=3, column=1, sticky=W)
            ttk.Label(perm, text="All").grid(row=0, column=2)
            ttk.Checkbutton(perm, text="Read", variable=binary[6], command=lambda: (permission_modification(entry[idx]["Checks"]["Permission to Set"], 4, binary[6]))).grid(row=1, column=2, sticky=W)
            ttk.Checkbutton(perm, text="Write", variable=binary[7], command=lambda: (permission_modification(entry[idx]["Checks"]["Permission to Set"], 2, binary[7]))).grid(row=2, column=2, sticky=W)
            ttk.Checkbutton(perm, text="Execute", variable=binary[8], command=lambda: (permission_modification(entry[idx]["Checks"]["Permission to Set"], 1, binary[8]))).grid(row=3, column=2, sticky=W)
            permissions = int(entry[idx]["Checks"]["Permission to Set"].get())
            for exp in range(0, 9, 1):
                bit = pow(2, 8 - exp)
                if permissions >= bit:
                    binary[exp].set(1)
                    permissions -= bit
                elif permissions == 0:
                    break
            c = r + 1
        elif t == "Service State":
            mod_frame.grid_columnconfigure(r, weight=1)
            ttk.OptionMenu(mod_frame, entry[idx]["Checks"][t], *["Running", "Running", "Stopped"]).grid(row=0, column=r, sticky=EW)
            c = r + 1
        elif t == "Service Start Mode":
            mod_frame.grid_columnconfigure(r, weight=1)
            ttk.OptionMenu(mod_frame, entry[idx]["Checks"][t], *["Auto", "Auto", "Manual", "Disabled"]).grid(row=0, column=r, sticky=EW)
            c = r + 1
        elif t == "User Name":
            mod_frame.grid_columnconfigure(r, weight=1)
            user_list = get_user_list()
            ttk.Combobox(mod_frame, textvariable=entry[idx]["Checks"][t], values=user_list).grid(row=0, column=r, sticky=EW)
            c = r + 1
        elif t == "Group Name":
            mod_frame.grid_columnconfigure(r, weight=1)
            group_list = get_group_list()
            ttk.Combobox(mod_frame, textvariable=entry[idx]["Checks"][t], values=group_list).grid(row=0, column=r, sticky=EW)
            c = r + 1
        else:
            mod_frame.grid_columnconfigure(r, weight=1)
            ttk.Entry(mod_frame, textvariable=entry[idx]["Checks"][t]).grid(row=0, column=r, sticky=EW)
            c = r + 1
    ttk.Button(mod_frame, text='X', width=8, command=lambda: (remove_row(entry, idx, mod_frame), Vulnerabilities.remove_from_table(name, idx))).grid(row=0, column=c, sticky=W)


def remove_row(entry, idx, widget):
    del entry[idx]
    widget.destroy()


def set_file_or_directory(var, switch, mode, binary):
    if switch.get() == 1:
        file = filedialog.askdirectory()
        var["File Path"].set(file)
    else:
        file = filedialog.askopenfilename()
        var["File Path"].set(file)
    if mode == "File Permissions":
        status = os.stat(file)
        current = bin(status.st_mode)[-9:]
        permissions = int(current, 2)
        var["Permission to Set"].set(permissions)
        for exp in range(0, 9, 1):
            bit = pow(2, 8 - exp)
            if permissions >= bit:
                binary[exp].set(1)
                permissions -= bit
            elif permissions == 0:
                return


def create_forensic():
    qHeader = 'This is a forensics question. Answer it below\n------------------------\n'
    qFooter = '\n\nANSWER: <TypeAnswerHere>'
    if vuln_settings["Forensic"][1]["Enabled"].get() == 1:
        q_num = 1
        for question in vuln_settings["Forensic"]:
            if question != 1:
                location = vuln_settings["Forensic"][question]["Checks"]["Location"].get()
                if location == "":
                    vuln_settings["Forensic"][question]["Checks"]["Location"].set(str(root.MenuSettings["Desktop"].get()) + 'Forensic Question ' + str(q_num) + '.txt')
                    location = vuln_settings["Forensic"][question]["Checks"]["Location"].get()
                g = open(location, 'w+')
                g.write(qHeader + vuln_settings["Forensic"][question]["Checks"]["Question"].get() + qFooter)
                g.close()


def commit_config():
    save_config()
    if os.getuid() != 0:
        messagebox.showerror('Administrator Access Needed', 'Please restart the configurator with sudo.')
        return

    infoWindow = Tk()
    info = SetupWindow(infoWindow)

    t1 = threading.Event()
    threading.Thread(target=worker, args=(info, t1)).start()

    infoWindow.after_idle(t1.set)
    infoWindow.mainloop()


def worker(frame, t):
    t.wait()

    progress = frame.progress
    timeStr = time.asctime()
    start = time.time()
    logging.info("{}\n".format(timeStr))

    if os.system("dpkg --list | grep lightdm"):
        logging.info("Installing lightDM...\n")
        with open('test.log', 'wb') as f:  # replace 'w' with 'wb' for Python 3
            for command in [['/bin/bash', '-c', "apt purge gdm3 -y"], ['/bin/bash', '-c', "apt install lightdm -y"], ['/bin/bash', '-c', "dpkg-reconfigure lightdm"]]:
                process = subprocess.Popen(command, stderr=subprocess.STDOUT, stdout=subprocess.PIPE)
                for line in iter(process.stdout.readline, b''):  # replace '' with b'' for Python 3
                    logging.info(line.decode(sys.stdout.encoding))
                    f.write(line)
                    progress["value"] += 1
        logging.info("Complete\n")
    else:
        logging.info("LightDM is already installed\n")
    progress["value"] = 90
    logging.info("Setting default settings...\n")
    generate_default_settings()
    progress["value"] += 1
    output_directory = '/usr/local/CyberPatriot/'
    logging.info("Setting output directory to '{}'\n".format(output_directory))
    progress["value"] += 1
    if not os.path.exists(output_directory):
        logging.info("{} did not exist. Creating...")
        os.makedirs(output_directory)
        logging.info("Complete\n")
    progress["value"] += 1
    logging.info("Coping CCC_logo.png to '{}'...".format(output_directory))
    shutil.copy(resource_path('CCC_logo.png'), os.path.join(output_directory, 'CCC_logo.png'))
    progress["value"] += 1
    logging.info("Complete\nCopying SoCalCCCC.png to '{}'...".format(output_directory))
    shutil.copy(resource_path('SoCalCCCC.png'), os.path.join(output_directory, 'SoCalCCCC.png'))
    progress["value"] += 1
    logging.info("Complete\nCopying Scoring_Engine_Logo_Linux_Icon.png to '{}'...".format(output_directory))
    shutil.copy(resource_path('Scoring_Engine_Logo_Linux_Icon.png'), os.path.join(output_directory, 'Scoring_Engine_Logo_Linux_Icon.png'))
    progress["value"] += 1
    logging.info("Complete\n")
    try:
        logging.info("Attempting to copy scoring_engine to '{}'...".format(output_directory))
        shutil.copy(resource_path('scoring_engine'), os.path.join(output_directory, 'scoring_engine'))
    except OSError as err:
        logging.info("{} Stopping csel...".format(err))
        os.system('systemctl stop csel')
        logging.info("Complete\nCopy scoring_engine to '{}'...".format(output_directory))
        shutil.copy(resource_path('scoring_engine'), os.path.join(output_directory, 'scoring_engine'))
    progress["value"] += 1
    logging.info("Complete\nChanging scoring_engine permissions...")
    os.chmod(os.path.join(output_directory, 'scoring_engine'), 0o777)
    progress["value"] += 1
    logging.info("Complete\n")
    install_systemd()
    progress["value"] = 100
    stop = time.time()
    logging.info("Finished after {} seconds\n".format(round(stop - start)))
    timeStr = time.asctime()
    logging.info("{}\n".format(timeStr))
    frame.exit.configure(state="enable")


def install_systemd():
    if not os.path.exists("/etc/systemd/system/csel.service"):
        shutil.copyfile(resource_path("systemd/csel.service"), "/etc/systemd/system/csel.service")
        os.system("systemctl daemon-reload")
        if os.system("systemctl enable csel") == 0:  # 0 is the normal exit code. other exit codes means a failure or error
            print("csel enabled on next boot. To enable now, run systemctl start csel")
        else:
            print("An error occurred")


def save_config():
    if os.system("systemctl status csel | grep '(running)'") == 0:
        os.system("systemctl stop csel")
    if "/Desktop/" not in root.MenuSettings["Desktop"].get():
        root.MenuSettings["Desktop"].set(os.path.expanduser("~") + "/Desktop/")
    create_forensic()
    tally()
    Settings.update_table(root.MenuSettings)
    for vuln in vuln_settings:
        Vulnerabilities.update_table(vuln, vuln_settings[vuln])


def tally():
    # Set tally scores
    tally_score = 0
    tally_vuln = 0
    for vuln in vuln_settings:
        if int(vuln_settings[vuln][1]["Enabled"].get()) == 1 and "Critical" not in vuln:
            if len(vuln_settings[vuln]) == 1:
                tally_vuln += 1
                tally_score += int(vuln_settings[vuln][1]["Points"].get())
            else:
                for settings in vuln_settings[vuln]:
                    if settings != 1:
                        tally_vuln += 1
                        tally_score += int(vuln_settings[vuln][settings]["Points"].get())
    root.MenuSettings["Tally Points"].set(tally_score)
    root.MenuSettings["Tally Vulnerabilities"].set(tally_vuln)


def get_user_list():
    user_list = ["root"]
    for user in check_output(['/bin/sh', '-c', "getent passwd | awk -F: '{ print $1,$3}'"]).decode('utf-8').split('\n'):
        name = user.split(' ') if user != '' else ['', 0]
        if 1000 <= int(name[1]) < 60000:
            user_list.append(name[0])
    return user_list


def get_group_list():
    group_list = check_output(['/bin/sh', '-c', "getent group | awk -F: '{ print $1}'"]).decode('utf-8').split('\n')
    return group_list


def show_error(self, *args):
    err = traceback.format_exception(*args)
    for i in err:
        if 'expected integer but got' in i:
            err = 'There is an integer error with one of the points'
    messagebox.showerror('Exception', err)


def resource_path(relative_path):
    """ https://stackoverflow.com/questions/7674790/bundling-data-files-with-pyinstaller-onefile/13790741#13790741
    Get absolute path to resource, works for dev and for PyInstaller """
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = sys._MEIPASS + "/extras"
        if not os.path.exists(os.path.join(base_path, relative_path)):
            base_path = os.path.abspath(".")
    except Exception:
        base_path = os.path.abspath(".")

    return os.path.join(base_path, relative_path)


def permission_modification(var, value, state):
    if var.get() is "":
        var.set(0)
    if state.get() == 1:
        var.set(int(var.get()) + value)
    else:
        var.set(int(var.get()) - value)


def set_default_themes(window):
    window.ttkStyle = ThemedStyle(window.winfo_toplevel())
    for theme in themeList:
        window.ttkStyle.set_theme(theme)
    window.ttkStyle.set_theme(root.MenuSettings["Style"].get())
    window.ttkStyle.theme_settings(themename="aquativo", settings={
        ".": {
            "configure": {
                "background": '#eff0f1'}
        },
        "TNotebook": {
            "configure": {
                "tabmargins": [2, 5, 2, 0]
            }
        },
        "TNotebook.Tab": {
            "configure": {
                "width": int(window.winfo_screenwidth() * 3 / 4 / 7),
                "anchor": 'center'
            }
        },
        "TLabel": {
            "configure": {
                "padding": '5 0',
                "justify": 'center',
                "wraplength": int(window.winfo_screenwidth() * 3 / 4 - 150)
            }
        },
        "TEntry": {
            "map": {
                "fieldbackground": [('disabled', '#a9acb2')]
            }
        },
        "TButton": {
            "configure": {
                "anchor": 'center',
                "width": '13'
            }
        }
    })
    window.ttkStyle.theme_settings(themename="black", settings={
        "TNotebook": {
            "configure": {
                "tabmargins": [2, 5, 2, 0]
            }
        },
        "TNotebook.Tab": {
            "configure": {
                "width": int(window.winfo_screenwidth() * 3 / 4 / 7),
                "anchor": 'center'
            }
        },
        "TLabel": {
            "configure": {
                "padding": '5 0',
                "justify": 'center',
                "wraplength": int(window.winfo_screenwidth() * 3 / 4 - 145)
            }
        },
        "TEntry": {
            "map": {
                "fieldbackground": [('disabled', '#868583')]
            }
        },
        "TButton": {
            "configure": {
                "anchor": 'center',
                "width": '13'
            }
        }
    })
    window.ttkStyle.theme_settings(themename="clearlooks", settings={
        "TNotebook": {
            "configure": {
                "tabmargins": [2, 5, 2, 0]
            }
        },
        "TNotebook.Tab": {
            "configure": {
                "width": int(window.winfo_screenwidth() * 3 / 4 / 7),
                "anchor": 'center'
            }
        },
        "TLabel": {
            "configure": {
                "padding": '5 0',
                "justify": 'center',
                "wraplength": int(window.winfo_screenwidth() * 3 / 4 - 180)
            }
        },
        "TEntry": {
            "map": {
                "fieldbackground": [('disabled', '#b0aaa4')]
            }
        },
        "TButton": {
            "configure": {
                "anchor": 'center',
                "width": '13'
            }
        }
    })
    window.ttkStyle.theme_settings(themename="elegance", settings={
        "TNotebook": {
            "configure": {
                "tabmargins": [2, 5, 2, 0]}},
        "TNotebook.Tab": {
            "configure": {
                "width": int(window.winfo_screenwidth() * 3 / 4 / 7),
                "anchor": 'center'}},
        "TLabel": {
            "configure": {
                "font": '8',
                "padding": '5 0',
                "justify": 'center',
                "wraplength": int(window.winfo_screenwidth() * 3 / 4 - 145)
            }
        },
        "TButton": {
            "configure": {
                "anchor": 'center',
                "width": '13'
            }
        }
    })
    window.ttkStyle.theme_settings(themename="equilux", settings={
        "TNotebook": {
            "configure": {
                "tabmargins": [2, 5, 2, 0]
            }
        },
        "TNotebook.Tab": {
            "configure": {
                "width": int(window.winfo_screenwidth() * 3 / 4 / 7),
                "anchor": 'center'
            }
        }, "TLabel": {
            "configure": {
                "padding": '5 0',
                "justify": 'center',
                "wraplength": int(window.winfo_screenwidth() * 3 / 4 - 145)
            },
            "map": {
                "foreground": [('disabled', '#5b5b5b')]
            }
        },
        "TButton": {
            "configure": {
                "anchor": 'center',
                "width": '13'
            }
        }
    })
    window.ttkStyle.theme_settings(themename="keramik", settings={
        "TNotebook": {
            "configure": {
                "tabmargins": [2, 5, 2, 0]
            }
        },
        "TNotebook.Tab": {
            "configure": {
                "width": int(window.winfo_screenwidth() * 3 / 4 / 7),
                "anchor": 'center'
            }
        },
        "TLabel": {
            "configure": {
                "padding": '5 0',
                "justify": 'center',
                "wraplength": int(window.winfo_screenwidth() * 3 / 4 - 145)
            }
        },
        "TButton": {
            "configure": {
                "anchor": 'center',
                "width": '13'
            }
        }
    })
    window.ttkStyle.theme_settings(themename="plastik", settings={
        "TNotebook": {
            "configure": {
                "tabmargins": [2, 5, 2, 0]
            }
        },
        "TNotebook.Tab": {
            "configure": {
                "width": int(window.winfo_screenwidth() * 3 / 4 / 7),
                "anchor": 'center'
            }
        },
        "TLabel": {
            "configure": {
                "padding": '5 0',
                "justify": 'center',
                "wraplength": int(window.winfo_screenwidth() * 3 / 4 - 145)
            }
        },
        "TButton": {
            "configure": {
                "anchor": 'center',
                "width": '13'
            }
        }
    })
    window.ttkStyle.theme_settings(themename="ubuntu", settings={
        "TNotebook": {
            "configure": {
                "tabmargins": [2, 5, 2, 0]
            }
        },
        "TNotebook.Tab": {
            "configure": {
                "width": int(window.winfo_screenwidth() * 3 / 4 / 7),
                "anchor": 'center'
            }
        },
        "TLabel": {
            "configure": {
                "padding": '5 0',
                "justify": 'center',
                "wraplength": int(window.winfo_screenwidth() * 3 / 4 - 170)
            },
            "map": {
                "foreground": [('disabled', '#c2c2c2')]
            }
        },
        "TButton": {
            "configure": {
                "anchor": 'center', "width": '13'
            }
        }
    })


def change_theme():
    root.ttkStyle.set_theme(root.MenuSettings["Style"].get())


def generate_HTML_export():
    save_config()
    default = False
    saveLocation = filedialog.asksaveasfilename(title='Select Save Location', defaultextension='.html', filetypes=(('Web Page', "*.html"), ('all files', '*.*')))
    head = '<!DOCTYPE html>\n<html>\n\t<head>\n\t\t<meta name="viewport" content="width=device-width, initial-scale=1">\n\t\t<style>\n\t\t\t* {box-sizing: border-box}\n\n\t\t\t.banner {\n\t\t\t\tborder-bottom: 1px solid #959b94;\n\t\t\t\tfont-size: 20px;\n\t\t\t}\n\n\t\t\tspan.true {\n\t\t\t\tbackground:green;\n\t\t\t\tcolor:white;\n\t\t\t}\n\n\t\t\tspan.false {\n\t\t\t\tbackground:red;\n\t\t\t\tcolor:white;\n\t\t\t}\n\n\t\t\t.tab {\n\t\t\t\tfloat: left;\n\t\t\t\tbackground-color: #f1f1f1;\n\t\t\t\twidth: 10%;\n\t\t\t\theight: 100%;\n\t\t\t}\n\n\t\t\t.tab button {\n\t\t\t\tdisplay: block;\n\t\t\t\tbackground-color: inherit;\n\t\t\t\tcolor: black;\n\t\t\t\tpadding: 22px 16px;\n\t\t\t\twidth: 100%;\n\t\t\t\tborder: none;\n\t\t\t\toutline: none;\n\t\t\t\ttext-align: left;\n\t\t\t\tcursor: pointer;\n\t\t\t\ttransition: 0.3s;\n\t\t\t\tfont-size: 25px;\n\t\t\t}\n\n\t\t\t.tab button:hover {\n\t\t\t\tbackground-color: #ddd;\n\t\t\t}\n\n\t\t\t.tab button.active {\n\t\t\t\tbackground-color: #ccc;\n\t\t\t}\n\n\t\t\t.tabcontent {\n\t\t\t\tfloat: left;\n\t\t\t\tpadding: 0px 12px;\n\t\t\t\twidth: 70%;\n\t\t\t\tborder-left: none;\n\t\t\t\theight: 300px;\n\t\t\t}\n\n\t\t\ttable.content {\n\t\t\t\twidth: 100%;\n\t\t\t\tborder-collapse: collapse;\n\t\t\t\ttext-align: center;\n\t\t\t}\n\n\t\t\ttr.head {\n\t\t\t\tfont-weight: bold;\n\t\t\t\tfont-size: 25px;\n\t\t\t}\n\n\t\t\ttr.label {\n\t\t\t\tborder: 1px solid black;\n\t\t\t\tfont-weight: bold;\n\t\t\t\tfont-size: 22px;\n\t\t\t}\n\n\t\t\ttd {\n\t\t\t\tborder: 1px solid black;\n\t\t\t}\n\n\t\t\ttd.banner {\n\t\t\t\tborder: none;\n\t\t\t\t}\n\t\t</style>\n\t</head>\n\t<body>\n\t\t<div class="banner">\n\t\t\t<table width="100%">\n\t\t\t\t<tr>\n\t\t\t\t\t<td class="banner" colspan="3">Save Location: ' + root.MenuSettings["Desktop"].get() + '</td>\n\t\t\t\t</tr>\n\t\t\t\t<tr>\n\t\t\t\t\t<td class="banner" width="20%">Silent Mode: <span class="'
    if root.MenuSettings["Silent Mode"].get():
        head += 'true">True'
    else:
        head += 'false">False'
    head += '</span></td>\n\t\t\t\t\t<td class="banner" width="20%">Server Mode: <span class="'
    if root.MenuSettings["Server Mode"].get():
        head += 'true">True</span></td>\n\t\t\t\t\t<td class="banner" width="60%">Sever Info: Ip:' + root.MenuSettings["Server Name"].get() + '\tUser Name: ' + root.MenuSettings["Server User"].get() + '\tPassword: ' + root.MenuSettings["Server Password"].get() + '</td>\n\t\t\t\t</tr>\n\t\t\t\t'
    else:
        head += 'false">False</span></td>\n\t\t\t\t'
    head += '<tr>\n\t\t\t\t\t<td class="banner" width="20%">Total Points: ' + root.MenuSettings["Tally Points"].get() + '<br>Total Vulnerabilities: ' + root.MenuSettings["Tally Vulnerabilities"].get() + '</td>\n\t\t\t\t</tr>\n\t\t\t</table>\n\t\t</div>\n\n\t\t'
    buttons = '\n\n\t\t<div class="tab">'
    body = ''

    categories = Categories.get_categories()
    for category in categories:
        vulnerabilities = Vulnerabilities.get_option_template_by_category(category.id)
        cat_tested = False
        temp_body = ''
        for vulnerability in vulnerabilities:
            settings = Vulnerabilities.get_option_table(vulnerability.name)
            if int(settings[1]["Enabled"].get()) == 1:
                cat_tested = True
                width = len(settings[1]["Checks"])
                temp_body += '\n\t\t\t<table class="content">\n\t\t\t\t<tr class="head">\n\t\t\t\t\t<td class="banner" colspan="' + str(width + 1) + '">' + vulnerability.name + '</td>\n\t\t\t\t</tr>\n\t\t\t\t<tr class="label">'
                temp_body += '\n\t\t\t\t\t<td width="5%">Points</td>'
                for check in settings[1]["Checks"]:
                    temp_body += '\n\t\t\t\t\t<td width="' + str(90 / width) + '%">' + check + '</td>'
                temp_body += '\n\t\t\t\t</tr>'
                for setting in settings:
                    if (width > 0 and setting != 1) or (width == 0):
                        temp_body += '\n\t\t\t\t<tr>\n\t\t\t\t\t<td width="5%">' + str(settings[setting]["Points"].get()) + '</td>'
                        for check in settings[setting]["Checks"]:
                            if check == "Permission to Set":
                                temp_body += '\n\t\t\t\t\t<td width="' + str(90 / width) + '%">\n\t\t\t\t\t\t<table class="content">\n\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t<td></td>\n\t\t\t\t\t\t\t\t<td><b>Read</b></td>\n\t\t\t\t\t\t\t\t<td><b>Write</b></td>\n\t\t\t\t\t\t\t\t<td><b>Execute</b></td>\n\t\t\t\t\t\t\t</tr>'
                                permissions = int(settings[setting]["Checks"]["Permission to Set"].get())
                                for exp in range(0, 9, 1):
                                    bit = pow(2, 8 - exp)
                                    if permissions >= bit:
                                        if exp == 0:
                                            temp_body += '\n\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t<td><b>Owner</b></td>\n\t\t\t\t\t\t\t\t<td><span class="true">True</span></td>'
                                        elif exp == 3:
                                            temp_body += '\n\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t<td><b>Group</b></td>\n\t\t\t\t\t\t\t\t<td><span class="true">True</span></td>'
                                        elif exp == 6:
                                            temp_body += '\n\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t<td><b>All</b></td>\n\t\t\t\t\t\t\t\t<td><span class="true">True</span></td>'
                                        elif exp % 3 == 1:
                                            temp_body += '\n\t\t\t\t\t\t\t\t<td><span class="true">True</span></td>'
                                        elif exp % 3 == 2:
                                            temp_body += '\n\t\t\t\t\t\t\t\t<td><span class="true">True</span></td>\n\t\t\t\t\t\t\t</tr>'
                                        permissions -= bit
                                    else:
                                        if exp == 0:
                                            temp_body += '\n\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t<td><b>Owner</b></td>\n\t\t\t\t\t\t\t\t<td><span class="false">False</span></td>'
                                        elif exp == 3:
                                            temp_body += '\n\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t<td><b>Group</b></td>\n\t\t\t\t\t\t\t\t<td><span class="false">False</span></td>'
                                        elif exp == 6:
                                            temp_body += '\n\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t<td><b>All</b></td>\n\t\t\t\t\t\t\t\t<td><span class="false">False</span></td>'
                                        elif exp % 3 == 1:
                                            temp_body += '\n\t\t\t\t\t\t\t\t<td><span class="false">False</span></td>'
                                        elif exp % 3 == 2:
                                            temp_body += '\n\t\t\t\t\t\t\t\t<td><span class="false">False</span></td>\n\t\t\t\t\t\t\t</tr>'
                                temp_body += '\n\t\t\t\t\t\t</table>'
                            else:
                                temp_body += '\n\t\t\t\t\t<td width="' + str(90 / width) + '%">' + str(settings[setting]["Checks"][check].get()) + '</td>'
                        temp_body += '\n\t\t\t\t</tr>'
                temp_body += '\n\t\t\t</table>'
        if cat_tested:
            buttons += '\n\t\t\t<button class="tablinks" onclick="openOptionSet(event, \'' + category.name + '\')"'
            if not default:
                default = True
                buttons += ' id="defaultOpen"'
            buttons += '>' + category.name + '</button>'
            body += '\n\n\t\t<div id="' + category.name + '" class="tabcontent">' + temp_body + '\n\t\t</div>\n'
    buttons += '\n\t\t</div>'
    body += '\n\n\t\t<script>\n\t\t\tfunction openOptionSet(evt, optionName) {\n\t\t\t\tvar i, tabcontent, tablinks;\n\t\t\t\ttabcontent = document.getElementsByClassName("tabcontent");\n\t\t\t\tfor (i = 0; i < tabcontent.length; i++) {\n\t\t\t\t\ttabcontent[i].style.display = "none";\n\t\t\t\t}\n\t\t\t\ttablinks = document.getElementsByClassName("tablinks");\n\t\t\t\tfor (i = 0; i < tablinks.length; i++) {\n\t\t\t\t\ttablinks[i].className = tablinks[i].className.replace(" active", "");\n\t\t\t\t}\n\t\t\t\tdocument.getElementById(optionName).style.display = "block";\n\t\t\t\tevt.currentTarget.className += " active";\n\t\t\t}\n\n\t\t\tdocument.getElementById("defaultOpen").click();\n\t\t</script>\n\t</body>\n</html>'
    head += buttons + body
    f = open(saveLocation, '+w')
    f.write(head)
    f.close()


'''def generate_CSV_export():
    save_config()
    saveLocation = filedialog.asksaveasfilename(title='Select Save Location', defaultextension='.csv', filetypes=(('Table File', "*.csv"), ('all files', '*.*')))
    with open(saveLocation, 'w', newline='') as file:
        writer = csv.writer(file)
        writer.writerow((root.MenuSettings["Desktop"].get(), "Silent Mode: " + "True" if root.MenuSettings["Silent Mode"].get() else "False", "Server Mode: " + "True" if root.MenuSettings["Server Mode"].get() else "False", "Server Name: " + root.MenuSettings["Server Name"].get(), "User Name: " + root.MenuSettings["Server User"].get(), "Password: " + root.MenuSettings["Server Password"].get(), "Total Points: " + root.MenuSettings["Tally Points"].get(), "Total Vulnerabilities: " + root.MenuSettings["Tally Vulnerabilities"].get()))
        category_list = []
        vuln_list = {}
        categories = Categories.get_categories()
        for category in categories:
            category_list.append(category.name)
            vuln_list.update({category.name: {"Names": [], "Options": {}}})
            vulnerabilities = Vulnerabilities.get_option_template_by_category(category.id)
            for vulnerability in vulnerabilities:
                vuln_list[category.name]["Names"].append(vulnerability.name)
                vuln_list[category.name]["Options"].update({vulnerability.name: []})
                settings = Vulnerabilities.get_option_table(vulnerability.name)
                # for setting in settings:
                vuln_list[category.name]["Options"][vulnerability.name].append("Points")
                for check in settings[1]["Checks"]:
                    vuln_list[category.name]["Options"][vulnerability.name].append(check)
        writer.writerow(category_list)
        for category in category_list:
            writer.writerow(vuln_list[category]["Names"])
            for vulnerability in vuln_list[category]["Options"]:
                writer.writerow(vuln_list[category]["Options"][vulnerability])'''


def generate_default_settings():
    if vuln_settings["Disable Guest"][1]["Enabled"].get():
        logging.info("Enabling the Guest account...")
        file = open("/usr/share/lightdm/lightdm.conf.d/50-ubuntu.conf", "r")
        line_content = file.readlines()
        file.close()
        file = open("/usr/share/lightdm/lightdm.conf.d/50-ubuntu.conf", "w+")
        setting_set = False
        for line in line_content:
            if "allow-guest=false" in line:
                line = "allow-guest=true\n"
                setting_set = True
            file.write(line)
        if not setting_set:
            file.write("allow-guest=true\n")
        file.close()
        logging.info("Complete\n")
    if vuln_settings["Disable User Greeter"][1]["Enabled"].get():
        logging.info("Enabling user greeter...")
        file = open("/usr/share/lightdm/lightdm.conf.d/50-ubuntu.conf", "r")
        line_content = file.readlines()
        file.close()
        file = open("/usr/share/lightdm/lightdm.conf.d/50-ubuntu.conf", "w+")
        setting_set = False
        for line in line_content:
            if "greeter-hide-users=true" in line:
                line = "greeter-hide-users=false\n"
                setting_set = True
            file.write(line)
        if not setting_set:
            file.write("greeter-hide-users=false\n")
        file.close()
        logging.info("Complete\n")
    if vuln_settings["Disable Auto Login"][1]["Enabled"].get():
        logging.info("Enabling auto login with a random user...")
        file = open("/etc/lightdm/lightdm.conf.d/50-ubuntu.conf", "r")
        line_content = file.readlines()
        file.close()
        file = open("/etc/lightdm/lightdm.conf.d/50-ubuntu.conf", "w+")
        setting_set = False
        username = random.sample(get_user_list(), k=1)
        logging.info("User chosen is: {}...".format(username))
        for line in line_content:
            if "autologin-user=" in line:
                line = "autologin-user=" + username + "\n"
                setting_set = True
            file.write(line)
        if not setting_set:
            file.write("autologin-user=" + username + "\n")
        file.close()
        logging.info("Complete\n")
    if vuln_settings["Unlock User"][1]["Enabled"].get():
        logging.info("Locking users...\n")
        for vuln in vuln_settings["Unlock User"]:
            if vuln != 1:
                logging.info("\tLocking {}...".format(vuln_settings["Unlock User"][vuln]["Checks"]["User Name"].get()))
                os.system("passwd -l " + vuln_settings["Unlock User"][vuln]["Checks"]["User Name"].get())
                logging.info("Locked\n")
        logging.info("Locking Complete\n")
    if vuln_settings["Lock User"][1]["Enabled"].get():
        logging.info("Unlocking users...\n")
        for vuln in vuln_settings["Lock User"]:
            if vuln != 1:
                logging.info("\tUnlocking {}...".format(vuln_settings["Lock User"][vuln]["Checks"]["User Name"].get()))
                os.system("passwd -u " + vuln_settings["Lock User"][vuln]["Checks"]["User Name"].get())
                logging.info("Unlocked\n")
        logging.info("Unlocking Complete\n")
    if vuln_settings["Disable Root"][1]["Enabled"].get():
        logging.info("Enabling Root and assining the password 'badPassword'...")
        os.system("usermod -p badPassword root")
        logging.info("Complete\n")


Tk.report_callback_exception = show_error

vulnerability_settings = {}
themeList = ["aquativo", "aquativo", "black", "clearlooks", "elegance", "equilux", "keramik", "plastik", "ubuntu"]

root = Config()
root.title('Configurator')
root.geometry("{0}x{1}+{2}+{3}".format(int(root.winfo_screenwidth() * 3 / 4), int(root.winfo_screenheight() * 2 / 3), int(root.winfo_screenwidth() / 9), int(root.winfo_screenheight() / 6)))
set_default_themes(root)

root.mainloop()

save_config()
