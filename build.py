import os
import sys
import shutil
import shlex
import tempfile
import getopt
from PyInstaller import __main__ as pyi


def move_project(src, dst):
    """ Move the output package to the desired path (default is output/ - set in script.js) """
    # Make sure the destination exists
    if not os.path.exists(dst):
        os.makedirs(dst)

    # Move all files/folders in dist/
    for file_or_folder in os.listdir(src):
        _dst = os.path.join(dst, file_or_folder)
        # If this already exists in the destination, delete it
        if os.path.exists(_dst):
            if os.path.isfile(_dst):
                os.remove(_dst)
            else:
                shutil.rmtree(_dst)
        # Move file
        shutil.move(os.path.join(src, file_or_folder), dst)


def convert(command, output_directory=(os.path.abspath('./'))):
    temporary_directory = tempfile.mkdtemp()
    dist_path = os.path.join(temporary_directory, 'application')
    build_path = os.path.join(temporary_directory, 'build')
    extra_args = ['--distpath', dist_path] + ['--workpath', build_path] + ['--specpath', temporary_directory]
    sys.argv = shlex.split(command) + extra_args
    pyi.run()
    move_project(dist_path, output_directory)
    shutil.rmtree(temporary_directory)


def help():
    print("[command] (-s|-c) [-o path/to/install]\n"
          "\t-c\tbuild configurator\n"
          "\t-s\tbuild score engine\n"
          "\t-d\tinstall the systemd service\n"
          "\t-o [path_to_install_dir]\tinstall to specified directory. Must be combined with -c and/or -s")


def install_systemd():
    shutil.copyfile("./systemd/csel.service", "/etc/systemd/system/csel.service")
    os.system("systemctl daemon-reload")
    if os.system("systemctl enable csel") == 0:  # 0 is the normal exit code. other exit codes means a failure or error
        print("csel enabled on next boot. To enable now, run systemctl start csel")
    else:
        print("An error occured")


def main(argv):
    scoringPath = os.path.abspath('scoring_engine.py')
    configPath = os.path.abspath('configurator.py')
    dbHandlerPath = os.path.abspath('db_handler.py')
    iconPath = os.path.abspath('Scoring_Engine_Logo_Linux_Icon.png')
    cccLogoPath = os.path.abspath('CCC_logo.png')
    SoCalPath = os.path.abspath('SoCalCCCC.png')
    enginePath = os.path.abspath('scoring_engine')
    servicePath = os.path.abspath('systemd')
    command_score = 'pyinstaller -y -F -c -i "' + iconPath + '" --add-data "' + dbHandlerPath + '":"." "' + scoringPath + '"'
    command_config = 'pyinstaller -y -F -c -i "' + iconPath + '" --add-data "' + dbHandlerPath + '":"." --add-data "' + cccLogoPath + '":"./extras" --add-data "' + SoCalPath + '":"./extras" --add-data "' + iconPath + '":"./extras" --add-data "' + enginePath + '":"./extras" --add-data "' + servicePath + '":"./extras/systemd" "' + configPath + '"'

    try:
        opts, args = getopt.getopt(argv, "hdcso:")
    except getopt.GetoptError:
        print("Incorrect usage")
        help()
        sys.exit(2)
    if not opts:
        print("build.py requires arguments to run")
        help()
    install_opts = []
    install_dest = ''
    for opt, arg in opts:
        if opt == '-h':
            help()
        elif opt == '-c':
            install_opts.append(opt)
        elif opt == '-s':
            install_opts.append(opt)
        elif opt == '-d':
            install_systemd()
        elif opt == '-o':
            install_opts.append(opt)
            install_dest = arg
        else:
            help()

    if '-s'in install_opts:
        if '-o' in install_opts and '-c' not in install_opts:
            print('Building the scoring engine and sending to custom directory')
            convert(command_score, output_directory=os.path.abspath(install_dest))
        else:
            print('Building the scoring engine and sending to current directory')
            convert(command_score)
    if '-c' in install_opts:
        if '-o' in install_opts:
            print('Building the configurator and sending to custom directory')
            convert(command_config, output_directory=os.path.abspath(install_dest))
        else:
            print('Building the configurator and sending to current directory')
            convert(command_config)

    # 'pyinstaller -y -F -w "/home/cyberpatriot/Desktop/configurator.py"'


if __name__ == "__main__":
    main(sys.argv[1:])
