import json
import os
import re
import sys
import time
import socket
import traceback
from subprocess import check_output
from inspect import getfullargspec
from tkinter import messagebox
from datetime import date
import db_handler


# Scoring Report creation
def draw_head():
    file = open(scoreIndex, 'w+')
    file.write('<!doctype html><html><head><title>CSEL Score Report</title><meta http-equiv="refresh" content="60"></head><body style="background-color:powderblue;">''\n')
    file.write('<table align="center" cellpadding="10"><tr><td><img src="/usr/local/CyberPatriot/CCC_logo.png"></td><td><div align="center"><H2>Cyberpatriot Scoring Engine:Linux v3.0</H2></div></td><td><img src="/usr/local/CyberPatriot/SoCalCCCC.png"></td></tr></table>If you see this wait a few seconds then refresh<br><H2>Your Score: #TotalScore#/' + str(menuSettings["Tally Points"]) + '</H2><H2>Vulnerabilities: #TotalVuln#/' + str(menuSettings["Tally Vulnerabilities"]) + '</H2><hr>')
    file.close()


def record_hit(name, points):
    global total_points, total_vulnerabilities
    write_to_html(('<p style="color:green">' + name if name.endswith('.') else (name + ". ") + ' (' + str(points) + ' points)</p>'))
    total_points += int(points)
    total_vulnerabilities += 1


def record_miss(name):
    if not menuSettings['Silent Mode']:
        write_to_html(('<p style="color:red">MISS ' + name + ' Issue</p>'))


def record_penalty(name, points):
    global total_points
    write_to_html(('<p style="color:red">' + name + ' (' + str(points) + ' points)</p>'))
    total_points -= int(points)


def draw_tail():
    write_to_html('<hr><div align="center"><b>Coastline Collage</b><br>Created by Shaun Martin, Anthony Nguyen, and Minh-Khoi Do</br><br>Feedback welcome: <a href="mailto:smartin94@student.cccd.edu?Subject=CSEL Scoring Engine" target="_top">smartin94@student.cccd.edu</a></div>')
    print(str(total_points) + ' / ' + str(menuSettings["Tally Points"]) + '\n' + str(total_vulnerabilities) + ' / ' + str(menuSettings["Tally Vulnerabilities"]))
    replace_section(scoreIndex, '#TotalScore#', str(total_points))
    replace_section(scoreIndex, '#TotalVuln#', str(total_vulnerabilities))
    replace_section(scoreIndex, 'If you see this wait a few seconds then refresh', '')

    if not os.path.exists(os.path.join(Desktop, "ScoreReport.html")):
        os.system('#!/bin/bash \ncd ' + Desktop + ' \nln /usr/local/CyberPatriot/ScoreReport.html')


# Extra Functions
def check_runas():
    if os.getuid() != 0:
        messagebox.showerror('Administrator Access Needed', 'Please make sure the scoring engine is running as admin. If this message keeps showing please contact the distributors.')
        exit()


def check_score():
    global total_points, total_vulnerabilities
    menuSettings["Current Vulnerabilities"] = total_vulnerabilities
    if total_points > menuSettings["Current Points"]:
        menuSettings["Current Points"] = total_points
        Settings.update_score(menuSettings)
        os.system("notify-send 'You gained points!!'")
    elif total_points < menuSettings["Current Points"]:
        menuSettings["Current Points"] = total_points
        Settings.update_score(menuSettings)
        os.system("notify-send 'You lost points!!'")
    if total_points == menuSettings["Tally Points"] and total_vulnerabilities == menuSettings["Tally Vulnerabilities"]:
        os.system("notify-send 'Congratulations you finished the image.'")


def write_to_html(message):
    file = open(scoreIndex, 'a')
    file.write(message)
    file.close()


def replace_section(loc, search, replace):
    lines = []
    with open(loc) as file:
        for line in file:
            line = line.replace(search, replace)
            lines.append(line)
    with open(loc, 'w') as file:
        for line in lines:
            file.write(line)


# Option Check
def forensic_question(vulnerability):
    for idx, vuln in enumerate(vulnerability):
        if vuln != 1:
            f = open(vulnerability[vuln]["Location"], 'r')
            content = f.read().splitlines()
            for c in content:
                if 'ANSWER:' in c:
                    if vulnerability[vuln]["Answers"] in c:
                        record_hit('Forensic question number ' + str(idx) + ' has been answered.', vulnerability[vuln]['Points'])
                    else:
                        record_miss('Forensic Question')


def disable_guest(vulnerability):
    if 'allow-guest=false' in lightdm_file:
        record_hit('The guest account has been disabled.', vulnerability[1]['Points'])
    else:
        record_miss('User Management')


def disable_root(vulnerability):
    if "!" in str(check_output(['/bin/sh', '-c', "getent shadow root"]).decode('utf-8')):
        record_hit('Root has been locked or disabled.', vulnerability[1]["Points"])
    else:
        record_miss('User Management')


def turn_on_firewall(vulnerability):
    if os.system("ufw status | grep 'Status: active'") == 0:
        record_hit('Firewall has been turned on.', vulnerability[1]["Points"])
    else:
        record_miss('Policy Management')
    return


def firewall_rules(vulnerability):
    content = load_file("/etc/ufw/user.rules")
    if content != "":
        copy = False
        copied = []
        for line in content:
            if '### RULES ###' in line:
                copy = True
            elif '### END RULES ###' in line:
                copy = False
            if copy and line != '':
                copied.append(line)
    else:
        return
    for vuln in vulnerability:
        if vuln != 1:
            if vulnerability[vuln]['Modification'] == 'Added':
                spotted = False
                for line in copied:
                    if '### tuple ###' in line and vulnerability[vuln]['Firewall Rule'] in line and vulnerability[vuln]['Port Number'] in line and vulnerability[vuln]['Traffic'] in line:
                        spotted = True
                        record_hit('Firewall rule \'' + vulnerability[vuln]['Firewall Rule'] + ' ' + vulnerability[vuln]['Port Number'] + '/' + vulnerability[vuln]['Traffic'] + '\' has been ' + vulnerability[vuln]['Modification'], vulnerability[vuln]["Points"])
                if not spotted:
                    record_miss('Policy Management')
            elif vulnerability[vuln]['Modification'] == 'Removed':
                spotted = False
                for line in copied:
                    if '### tuple ###' in line and vulnerability[vuln]['Firewall Rule'] in line and vulnerability[vuln]['Port Number'] in line and vulnerability[vuln]['Traffic'] in line:
                        spotted = True
                if spotted:
                    record_miss('Policy Management')
                else:
                    record_hit('Firewall rule \'' + vulnerability[vuln]['Firewall Rule'] + ' ' + vulnerability[vuln]['Port Number'] + '/' + vulnerability[vuln]['Traffic'] + '\' has been ' + vulnerability[vuln]['Modification'], vulnerability[vuln]["Points"])


def minimum_password_age(vulnerability):
    min_age = int(re.search(r"(?<=PASS_MIN_DAYS\s)(\d+)", login_file).group(0) if re.search(r"(?<=PASS_MIN_DAYS\s)(\d+)", login_file) else 0)
    if 30 <= min_age <= 60:
        record_hit('Minimum password age is set to 30-60.', vulnerability[1]["Points"])
    else:
        record_miss('Policy Management')


def maximum_password_age(vulnerability):
    max_age = int(re.search(r"(?<=PASS_MAX_DAYS\s)(\d+)", login_file).group(0) if re.search(r"(?<=PASS_MAX_DAYS\s)(\d+)", login_file) else 0)
    if 60 <= max_age <= 90:
        record_hit('Maximum password age is set to 60-90.', vulnerability[1]["Points"])
    else:
        record_miss('Policy Management')


def maximum_login_tries(vulnerability):
    max_retry = int(re.search(r"(?<=LOGIN_RETRIES\s{2})(\d+)", login_file).group(0) if re.search(r"(?<=LOGIN_RETRIES\s{2})(\d+)", login_file) else 0)
    if 5 <= max_retry <= 10:
        record_hit('Maximum login tries is set to 5-10.', vulnerability[1]["Points"])
    else:
        record_miss('Policy Management')


def minimum_password_length(vulnerability):
    min_len = int(re.search(r"(?<=minlen=)(-?\d+)", common_password_file).group(0) if re.search(r"(?<=minlen=)(-?\d+)", common_password_file) else 0)
    if min_len >= 10:
        record_hit('Minimum password length is set to 10+.', vulnerability[1]["Points"])
    else:
        record_miss('Policy Management')


def password_history(vulnerability):
    pass_hist = int(re.search(r"(?<=remember=)(-?\d+)", common_password_file).group(0) if re.search(r"(?<=remember=)(-?\d+)", common_password_file) else 0)
    if pass_hist >= 5:
        record_hit('Password history size is set to 5-10.', vulnerability[1]["Points"])
    else:
        record_miss('Policy Management')


def password_complexity(vulnerability):
    pass_comp_l = int(re.search(r"(?<=lcredit=)(-?\d+)", common_password_file).group(0) if re.search(r"(?<=lcredit=)(-?\d+)", common_password_file) else 0)
    pass_comp_u = int(re.search(r"(?<=ucredit=)(-?\d+)", common_password_file).group(0) if re.search(r"(?<=ucredit=)(-?\d+)", common_password_file) else 0)
    pass_comp_d = int(re.search(r"(?<=dcredit=)(-?\d+)", common_password_file).group(0) if re.search(r"(?<=dcredit=)(-?\d+)", common_password_file) else 0)
    pass_comp_o = int(re.search(r"(?<=ocredit=)(-?\d+)", common_password_file).group(0) if re.search(r"(?<=ocredit=)(-?\d+)", common_password_file) else 0)
    if [pass_comp_l != 0, pass_comp_u != 0, pass_comp_d != 0, pass_comp_o != 0].count(True) >= 3:
        record_hit('Password complexity has been enabled.', vulnerability[1]["Points"])
    else:
        record_miss('Policy Management')


def disable_user_greeter(vulnerability):
    if 'greeter-hide-users=true' in lightdm_file:
        record_hit('User Greeter has been disabled.', vulnerability[1]["Points"])
    else:
        record_miss('Policy Management')


def disable_auto_login(vulnerability):
    if not re.search('autologin-user=.+', lightdm_file):
        record_hit('Auto Login has been disabled.', vulnerability[1]["Points"])
    else:
        record_miss('Policy Management')


def critical_users(vulnerability):
    for vuln in vulnerability:
        if vuln != 1:
            if os.system('getent passwd ' + vulnerability[vuln]["User Name"].lower()) > 0:
                record_penalty(vulnerability[vuln]["User Name"] + ' was removed.', vulnerability[vuln]["Points"])


def add_user(vulnerability):
    for vuln in vulnerability:
        if vuln != 1:
            if os.system('getent passwd ' + vulnerability[vuln]["User Name"].lower()) == 0:
                record_hit(vulnerability[vuln]["User Name"] + ' has been added.', vulnerability[vuln]["Points"])
            else:
                record_miss('User Management')


def remove_user(vulnerability):
    for vuln in vulnerability:
        if vuln != 1:
            if os.system('getent passwd ' + vulnerability[vuln]["User Name"].lower()) > 0:
                record_hit(vulnerability[vuln]["User Name"] + ' has been removed.', vulnerability[vuln]["Points"])
            else:
                record_miss('User Management')


def add_admin(vulnerability):
    for vuln in vulnerability:
        if vuln != 1:
            if os.system('getent group sudo | grep ' + vulnerability[vuln]["User Name"].lower()) == 0:
                record_hit(vulnerability[vuln]["User Name"] + ' has been promoted to administrator.', vulnerability[vuln]["Points"])
            else:
                record_miss('User Management')


def remove_admin(vulnerability):
    for vuln in vulnerability:
        if vuln != 1:
            if os.system('getent group sudo | grep ' + vulnerability[vuln]["User Name"].lower()) > 0:
                record_hit(vulnerability[vuln]["User Name"] + ' has been demoted to standard user.', vulnerability[vuln]["Points"])
            else:
                record_miss('User Management')


def add_user_to_group(vulnerability):
    for vuln in vulnerability:
        if vuln != 1:
            if os.system('getent group ' + vulnerability[vuln]['Group Name'].lower() + ' | grep ' + vulnerability[vuln]["User Name"].lower()) == 0:
                record_hit(vulnerability[vuln]["User Name"] + ' is in the ' + vulnerability[vuln]['Group Name'] + ' group.', vulnerability[vuln]["Points"])
            else:
                record_miss('User Management')


def remove_user_from_group(vulnerability):
    for vuln in vulnerability:
        if vuln != 1:
            if os.system('getent group ' + vulnerability[vuln]['Group Name'].lower() + ' | grep ' + vulnerability[vuln]["User Name"].lower()) > 0:
                record_hit(vulnerability[vuln]["User Name"] + ' is no longer in the ' + vulnerability[vuln]['Group Name'] + ' group.', vulnerability[vuln]["Points"])
            else:
                record_miss('User Management')


def user_change_password(vulnerability):
    current_date = str(date.today().strftime("%b %d, %Y"))
    current_m = str(date.today().strftime("%b"))
    current_d = int(date.today().strftime("%d"))
    current_y = int(date.today().strftime("%Y"))
    for vuln in vulnerability:
        if vuln != 1:
            last_changed = str(check_output(['/bin/sh', '-c', "chage -l " + vulnerability[vuln]["User Name"].lower() + " | grep -oP '(?<=Last password change\\s{5}:\\s)([a-zA-Z]+ \\d+, \\d+)'"]).decode('utf-8'))
            last_changed_m = last_changed.split(' ', 1)[0]
            last_changed_d = int(last_changed.split(' ', 1)[1].split(', ', 1)[0])
            last_changed_y = int(last_changed.split(' ', 1)[1].split(', ', 1)[1])
            if current_date == last_changed or (current_m == last_changed_m and current_d <= last_changed_d and current_y == last_changed_y):
                record_hit(vulnerability[vuln]["User Name"] + '\'s password was changed.', vulnerability[vuln]["Points"])
            else:
                record_miss('User Management')


def unlock_user(vulnerability):
    for vuln in vulnerability:
        if vuln != 1:
            if "!" not in str(check_output(['/bin/sh', '-c', "getent shadow " + vulnerability[vuln]["User Name"]]).decode('utf-8')):
                record_hit(vulnerability[vuln]["User Name"] + ' has been unlocked.', vulnerability[vuln]["Points"])
            else:
                record_miss('User Management')


def lock_user(vulnerability):
    for vuln in vulnerability:
        if vuln != 1:
            if "!" in str(check_output(['/bin/sh', '-c', "getent shadow " + vulnerability[vuln]["User Name"]]).decode('utf-8')):
                record_hit(vulnerability[vuln]["User Name"] + ' has been locked.', vulnerability[vuln]["Points"])
            else:
                record_miss('User Management')


def enable_ssh_root_login(vulnerability):
    sshdConfig = load_file("/etc/ssh/sshd_config")
    if sshdConfig != "":
        for line in sshdConfig:
            if "PermitRootLogin" in line:
                if '#' not in line and ("yes" in line or "true" in line):
                    record_hit('Root login has been enabled for ssh terminals.', vulnerability[1]["Points"])
                else:
                    record_miss('Policy Management')


def disable_ssh_root_login(vulnerability):
    sshdConfig = load_file("/etc/ssh/sshd_config")
    if sshdConfig != "":
        for line in sshdConfig:
            if "PermitRootLogin" in line:
                if '#' not in line and ("no" in line or "false" in line):
                    record_hit('Root login has been disabled for ssh terminals.', vulnerability[1]["Points"])
                else:
                    record_miss('Policy Management')


def check_startup(vulnerability):
    for vuln in vulnerability:
        if vuln != 1:
            os.system('ls /etc/ > startup')
            startup_file = load_file("startup").splitlines()
            for file in startup_file:
                if re.search('rc.\\.d', file):
                    if not os.path.exists('/etc/' + file + vulnerability[vuln]['Program Name']):
                        record_hit(vulnerability[vuln]['Program Name'] + ' Removed from Startup', vulnerability[vuln]["Points"])
                    else:
                        record_miss('Program Management')


def update_check_period(vulnerability):
    check_period = int(re.search(r"(?<=APT::Periodic::Update-Package-Lists \")\d+", auto_upgrades_file).group(0) if re.search(r"(?<=APT::Periodic::Update-Package-Lists \")\d+", auto_upgrades_file) else 0)
    if check_period == 1:
        record_hit('Update check period is set to 1 in /etc/apt/apt.conf.d/20auto-upgrades.', vulnerability[1]["Points"])
    else:
        record_miss('Policy Management')


def update_auto_install(vulnerability):
    auto_download = int(re.search(r"(?<=APT::Periodic::Download-Upgradeable-Packages \")\d+", auto_upgrades_file).group(0) if re.search(r"(?<=APT::Periodic::Download-Upgradeable-Packages \")\d+", auto_upgrades_file) else 0)
    auto_install = int(re.search(r"(?<=APT::Periodic::Unattended-Upgrade \")\d+", auto_upgrades_file).group(0) if re.search(r"(?<=APT::Periodic::Unattended-Upgrade \")\d+", auto_upgrades_file) else 0)
    if auto_download == 1 and auto_install == 1:
        record_hit('Update download and install are set to 1 in /etc/apt/apt.conf.d/20auto-upgrades.', vulnerability[1]["Points"])
    else:
        record_miss('Policy Management')


def monitor_ports(vulnerability):
    for vuln in vulnerability:
        if vuln != 1:
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            try:
                sock.connect((vulnerability[vuln]['Local Address'], int(vulnerability[vuln]['Port Number'])))
                status = "open/listening"
            except:
                status = "closed"
            sock.close()
            if vulnerability[vuln]['Port State'] == status:
                record_hit('Port number ' + vulnerability[vuln]['Port Number'] + ' is set to ' + status + '.', vulnerability[vuln]["Points"])
            else:
                record_miss('Policy Management')


def cron_tab(vulnerability):
    for vuln in vulnerability:
        if vuln != 1:
            if os.system('crontab -u ' + vulnerability[vuln]["User Name"].lower() + ' -l | grep ^# | grep \'' + vulnerability[vuln]["Task Name"] + '\'') == 0:
                record_hit('Cron job ' + vulnerability[vuln]["Task Name"] + ' in ' + vulnerability[vuln]["User Name"] + ' has been commented out.', vulnerability[vuln]["Points"])
            else:
                if os.system('crontab -u ' + vulnerability[vuln]["User Name"] + ' -l | grep \'' + vulnerability[vuln]["Task Name"] + '\'') > 0:
                    record_hit('Cron job ' + vulnerability[vuln]["Task Name"] + ' in ' + vulnerability[vuln]["User Name"] + ' has been deleted.', vulnerability[vuln]["Points"])
                else:
                    record_miss('Program Management')


def secure_sudoers(vulnerability):
    for vuln in vulnerability:
        if vuln != 1:
            if os.system('grep "' + vulnerability[vuln]["User Name"] + '" /etc/sudoers') > 0:
                record_hit(vulnerability[vuln]["User Name"] + ' has been removed from the /etc/sudoers file.', vulnerability[vuln]["Points"])
            else:
                record_miss('Account Management')


def add_text_to_file(vulnerability):
    for vuln in vulnerability:
        if vuln != 1:
            fp = load_file(vulnerability[vuln]['File Path'])
            p = re.compile(vulnerability[vuln]['Text to Add'], re.I | re.M)
            if p.search(fp):
                record_hit(vulnerability[vuln]['Text to Add'] + ' has been added to ' + vulnerability[vuln]['File Path'], vulnerability[vuln]["Points"])
            else:
                record_miss('File Management')


def remove_text_from_file(vulnerability):
    for vuln in vulnerability:
        if vuln != 1:
            fp = load_file(vulnerability[vuln]['File Path'])
            p = re.compile(vulnerability[vuln]['Text to Remove'], re.I | re.M)
            if not (p.search(fp)):
                record_hit(vulnerability[vuln]['Text to Remove'] + ' has been removed from ' + vulnerability[vuln]['File Path'], vulnerability[vuln]["Points"])
            else:
                record_miss('File Management')


def file_permissions(vulnerability):
    for vuln in vulnerability:
        if vuln != 1:
            if os.path.exists(vulnerability[vuln]['File Path']):
                status = os.stat(vulnerability[vuln]['File Path'])
                current = bin(status.st_mode)[-9:]
                permissions = bin(vulnerability[vuln]['Permission to Set'])[-9:]
                if permissions == current:
                    temp = "{0}{1}{2}{3}{4}{5}{6}{7}{8}".format('r' if permissions[0] == '1' else '-', 'w' if permissions[1] == '1' else '-', 'x' if permissions[2] == '1' else '-', 'r' if permissions[3] == '1' else '-', 'w' if permissions[4] == '1' else '-', 'x' if permissions[5] == '1' else '-', 'r' if permissions[6] == '1' else '-', 'w' if permissions[7] == '1' else '-', 'x' if permissions[8] == '1' else '-')
                    record_hit('The permissions for ' + vulnerability[vuln]['File Path'] + ' has been set to ' + temp, vulnerability[vuln]["Points"])
                else:
                    record_miss('File Management')
            else:
                record_miss('File Management')


def check_hosts(vulnerability):
    for vuln in vulnerability:
        if vuln != 1:
            if os.system('grep ' + vulnerability[vuln]['Text'] + ' /etc/hosts') > 0:
                record_hit(vulnerability[vuln]['Text'] + ' removed from /etc/hosts.', vulnerability[vuln]["Points"])
            else:
                record_miss('File Management')


def critical_services(vulnerability):
    for vuln in vulnerability:
        if vuln != 1:
            if os.system('systemctl status ' + vulnerability[vuln]['Service Name'] + ' | grep "(' + vulnerability[vuln]['Service Status'] + ')"'):
                record_penalty(vulnerability[vuln]['Service Name'] + ' was changed.', vulnerability[vuln]["Points"])


def services(vulnerability):
    for vuln in vulnerability:
        if vuln != 1:
            if os.system('systemctl status ' + vulnerability[vuln]['Service Name'] + ' | grep "(' + vulnerability[vuln]['Service Status'] + ')"'):
                record_hit(vulnerability[vuln]['Service Name'] + ' has been set to ' + vulnerability[vuln]['Service Status'], vulnerability[vuln]["Points"])
            else:
                record_miss('Program Management')


def critical_programs(vulnerability):
    for vuln in vulnerability:
        if vuln != 1:
            if os.system('apt-cache policy ' + vulnerability[vuln]['Program Name'] + ' | grep "Installed: (none)"') > 0:
                record_penalty(vulnerability[vuln]['Program Name'] + ' was uninstalled.', vulnerability[vuln]["Points"])


def good_program(vulnerability):
    for vuln in vulnerability:
        if vuln != 1:
            if os.system('apt-cache policy ' + vulnerability[vuln]['Program Name'] + ' | grep "Installed: (none)"') > 0:
                record_hit(vulnerability[vuln]['Program Name'] + ' is installed', vulnerability[vuln]["Points"])
            else:
                record_miss('Program Management')


def bad_program(vulnerability):
    for vuln in vulnerability:
        if vuln != 1:
            if os.system('apt-cache policy ' + vulnerability[vuln]['Program Name'] + ' | grep "Installed: (none)"') == 0:
                record_hit(vulnerability[vuln]['Program Name'] + ' is uninstalled', vulnerability[vuln]["Points"])
            else:
                record_miss('Program Management')


def update_program(vulnerability):
    for vuln in vulnerability:
        if vuln != 1:
            current = str(check_output(['/bin/sh', '-c', "apt-cache policy " + vulnerability[vuln]['Program Name'] + " | grep Installed:"]).decode('utf-8')).split(': ')[1]
            avaliable = str(check_output(['/bin/sh', '-c', "apt-cache policy " + vulnerability[vuln]['Program Name'] + " | grep Candidate:"]).decode('utf-8')).split(': ')[1]
            if current == avaliable:
                record_hit(vulnerability[vuln]['Program Name'] + ' is updated', vulnerability[vuln]["Points"])
            else:
                record_miss('Program Management')


def anti_virus(vulnerability):
    if os.system('apt-cache policy clamav | grep "Installed: (none)"') > 0:
        record_hit('Clamav has been installed.', vulnerability[1]["Points"])
    else:
        record_miss('Security')


def bad_file(vulnerability):
    for vuln in vulnerability:
        if vuln != 1:
            if not os.path.exists(vulnerability[vuln]['File Path']):
                record_hit('The item ' + vulnerability[vuln]['File Path'] + ' has been removed.', vulnerability[vuln]["Points"])
            else:
                record_miss('File Management')


def load_file(filename):
    if os.path.exists(filename):
        file = open(filename, 'r')
        content = file.read()
        file.close()
    else:
        content = ""
    return content


def no_scoring_available(name):
    messagebox.showerror(("No scoring for:", name), ("There is no scoring definition for", name, ". Please remove this option if you are the image creator, if you are a competitor ignore this message."))


def account_management(vulnerabilities):
    write_to_html('<H3>USER MANAGEMENT</H3>')
    vulnerability_def = {"Disable Guest": disable_guest, "Disable Root": disable_root, "Add Admin": add_admin, "Remove Admin": remove_admin, "Add User to Group": add_user_to_group, "Remove User from Group": remove_user_from_group, "Add User": add_user, "Remove User": remove_user, "User Change Password": user_change_password, "Unlock User": unlock_user, "Lock User": lock_user}
    for vuln in vulnerabilities:
        vulnerability = Vulnerabilities.get_option_table(vuln.name, False)
        if "Critical" in vuln.name:
            critical_items.append(vuln)
        elif vulnerability[1]["Enabled"]:
            if len(getfullargspec(vulnerability_def[vuln.name]).args) == 1:
                vulnerability_def[vuln.name](vulnerability if "vulnerability" in getfullargspec(vulnerability_def[vuln.name]).args else vuln.name)
            else:
                vulnerability_def[vuln.name](vulnerability, vuln.name)


def local_policies(vulnerabilities):
    write_to_html('<H3>SECURITY POLICIES</H3>')
    vulnerability_def = {"Secure Sudoers": secure_sudoers, "Disable Auto Login": disable_auto_login, "Disable User Greeter": disable_user_greeter, "Enable ssh root login": enable_ssh_root_login, "Disable ssh root login": disable_ssh_root_login, "Turn On Firewall": turn_on_firewall, "Firewall Rules": firewall_rules, "Monitor Ports": monitor_ports, "Minimum Password Age": minimum_password_age, "Maximum Password Age": maximum_password_age, "Maximum Login Tries": maximum_login_tries, "Minimum Password Length": minimum_password_length, "Maximum Login Tries": maximum_login_tries, "Password History": password_history, "Password Complexity": password_complexity}
    for vuln in vulnerabilities:
        vulnerability = Vulnerabilities.get_option_table(vuln.name, False)
        if vulnerability[1]["Enabled"]:
            if len(getfullargspec(vulnerability_def[vuln.name]).args) == 1:
                vulnerability_def[vuln.name](vulnerability if "vulnerability" in getfullargspec(vulnerability_def[vuln.name]).args else vuln.name)
            else:
                vulnerability_def[vuln.name](vulnerability, vuln.name)


def program_management(vulnerabilities):
    write_to_html('<H3>PROGRAMS</H3>')
    vulnerability_def = {"Good Program": good_program, "Bad Program": bad_program, "Update Program": update_program, "Services": services}
    for vuln in vulnerabilities:
        vulnerability = Vulnerabilities.get_option_table(vuln.name, False)
        if "Critical" in vuln.name:
            critical_items.append(vuln)
        elif vulnerability[1]["Enabled"]:
            if len(getfullargspec(vulnerability_def[vuln.name]).args) == 1:
                vulnerability_def[vuln.name](vulnerability if "vulnerability" in getfullargspec(vulnerability_def[vuln.name]).args else vuln.name)
            else:
                vulnerability_def[vuln.name](vulnerability, vuln.name)


def file_management(vulnerabilities):
    write_to_html('<H3>FILE MANAGEMENT</H3>')
    vulnerability_def = {"Forensic": forensic_question, "Bad File": bad_file, "Check Hosts": check_hosts, "Add Text to File": add_text_to_file, "Remove Text From File": remove_text_from_file, "File Permissions": file_permissions, "File Share": no_scoring_available}
    for vuln in vulnerabilities:
        vulnerability = Vulnerabilities.get_option_table(vuln.name, False)
        if vulnerability[1]["Enabled"]:
            if len(getfullargspec(vulnerability_def[vuln.name]).args) == 1:
                vulnerability_def[vuln.name](vulnerability if "vulnerability" in getfullargspec(vulnerability_def[vuln.name]).args else vuln.name)
            else:
                vulnerability_def[vuln.name](vulnerability, vuln.name)


def miscellaneous(vulnerabilities):
    write_to_html('<H3>MISCELLANEOUS</H3>')
    vulnerability_def = {"Anti-Virus": anti_virus, "Update Check Period": update_check_period, "Update Auto Install": update_auto_install, "Disable IPv6 Login": no_scoring_available, "Update Kernal": no_scoring_available, "Cron Tab": cron_tab, "Check Startup": check_startup}
    for vuln in vulnerabilities:
        vulnerability = Vulnerabilities.get_option_table(vuln.name, False)
        if vulnerability[1]["Enabled"]:
            if len(getfullargspec(vulnerability_def[vuln.name]).args) == 1:
                vulnerability_def[vuln.name](vulnerability if "vulnerability" in getfullargspec(vulnerability_def[vuln.name]).args else vuln.name)
            else:
                vulnerability_def[vuln.name](vulnerability, vuln.name)


def critical_functions(vulnerabilities):
    write_to_html('<H4>Critical Functions:</H4>')
    vulnerability_def = {"Critical Users": critical_users, "Critical Programs": critical_programs, "Critical Services": critical_services}
    for vuln in vulnerabilities:
        vulnerability = Vulnerabilities.get_option_table(vuln.name, False)
        if vulnerability[1]["Enabled"]:
            vulnerability_def[vuln.name](vulnerability)


print("Loading Settings")
try:
    Settings = db_handler.Settings()
    menuSettings = Settings.get_settings(False)
    Categories = db_handler.Categories()
    categories = Categories.get_categories()
    Vulnerabilities = db_handler.OptionTables()
    Vulnerabilities.initialize_option_table()
except:
    f = open('scoring_engine.log', 'w')
    e = traceback.format_exc()
    if "KeyboardInterrupt" in e:
        sys.exit()
    f.write(str(e))
    f.close()
    messagebox.showerror('Crash Report', 'The scoring engine has stopped working, a log has been saved to ' + os.path.abspath('scoring_engine.log'))
    sys.exit()

total_points = 0
total_vulnerabilities = 0
prePoints = 0
category_def = {"Account Management": account_management, "Local Policy": local_policies, "Program Management": program_management, "File Management": file_management, "Miscellaneous": miscellaneous}
Desktop = menuSettings["Desktop"]
index = '/usr/local/CyberPatriot/'
scoreIndex = index + 'ScoreReport.html'

# --------- Main Loop ---------#
print("Running Checks")
check_runas()
while True:
    print("Initializing Variables and Running Scrips(~20 seconds)")
    try:
        total_points = 0
        total_vulnerabilities = 0
        critical_items = []
        auto_upgrades_file = load_file("/etc/apt/apt.conf.d/20auto-upgrades")
        lightdm_file = load_file("/etc/lightdm/lightdm.conf.d/50-ubuntu.conf")
        if lightdm_file == "":
            lightdm_file = load_file("/etc/lightdm/lightdm.conf")
        common_password_file = load_file("/etc/pam.d/common-password")
        login_file = load_file("/etc/login.defs")
        time.sleep(20)
        print("Building Report Head")
        draw_head()
        for category in categories:
            print("Checking", category.name, "Options")
            category_def[category.name](Vulnerabilities.get_option_template_by_category(category.id))
        print("Checking Critical Functions")
        critical_functions(critical_items)
        print("Checking Score")
        check_score()
        print("Building Report Tail")
        draw_tail()
        print("Finished...Looping in 30 Seconds")
        time.sleep(30)
    except:
        f = open('scoring_engine.log', 'w')
        e = traceback.format_exc()
        if "KeyboardInterrupt" in e:
            sys.exit()
        f.write(str(e))
        f.close()
        messagebox.showerror('Crash Report', 'The scoring engine has stopped working, a log has been saved to ' + os.path.abspath('scoring_engine.log'))
        sys.exit()
