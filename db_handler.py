import sys, os

from sqlalchemy import orm
from sqlalchemy.ext.declarative import declarative_base
import sqlalchemy as sa

from tkinter import StringVar, IntVar

try:
    # PyInstaller creates a temp folder and stores path in _MEIPASS
    base_path = sys._MEIPASS
    passExist = True
    base_path = '/usr/local/CyberPatriot/'
    if not os.path.exists(base_path):
        os.makedirs(base_path)
except Exception:
    base_path = os.path.abspath(".")
    passExist = False
db = os.path.join(base_path, 'save_data.db')

base = declarative_base()
engine = sa.create_engine('sqlite:///' + db)
base.metadata.bind = engine
session = orm.scoped_session(orm.sessionmaker())(bind=engine)


class SettingsModel(base):
    __tablename__ = "Settings"
    id = sa.Column(sa.Integer, primary_key=True)
    style = sa.Column(sa.String(128), nullable=False, default="black")
    desktop = sa.Column(sa.Text, nullable=False, default=" ")
    silent_mode = sa.Column(sa.Boolean, nullable=False, default=False)
    server_mode = sa.Column(sa.Boolean, nullable=False, default=False)
    server_name = sa.Column(sa.String(255))
    server_user = sa.Column(sa.String(255))
    server_pass = sa.Column(sa.String(128))
    tally_points = sa.Column(sa.Integer, nullable=False, default=0)
    tally_vuln = sa.Column(sa.Integer, nullable=False, default=0)
    current_points = sa.Column(sa.Integer, nullable=False, default=0)
    current_vuln = sa.Column(sa.Integer, nullable=False, default=0)

    def __init__(self, **kwargs):
        super(SettingsModel, self).__init__(**kwargs)


class Settings:
    def __init__(self):
        if session.query(SettingsModel).scalar() is None:
            self.settings = SettingsModel()
            session.add(self.settings)
            session.commit()
        else:
            self.settings = session.query(SettingsModel).one()

    def get_settings(self, config=True):
        if config:
            return {"Style": StringVar(value=self.settings.style), "Desktop": StringVar(value=self.settings.desktop), "Silent Mode": StringVar(value=self.settings.silent_mode), "Server Mode": StringVar(value=self.settings.server_mode), "Server Name": StringVar(value=self.settings.server_name), "Server User": StringVar(value=self.settings.server_user), "Server Password": StringVar(value=self.settings.server_pass), "Tally Points": StringVar(value=self.settings.tally_points), "Tally Vulnerabilities": StringVar(value=self.settings.tally_vuln)}
        else:
            return {"Desktop": self.settings.desktop, "Silent Mode": self.settings.silent_mode, "Server Mode": self.settings.server_mode, "Server Name": self.settings.server_name, "Server User": self.settings.server_user, "Server Password": self.settings.server_pass, "Tally Points": self.settings.tally_points, "Tally Vulnerabilities": self.settings.tally_vuln, "Current Points": self.settings.current_points, "Current Vulnerabilities": self.settings.current_vuln}

    def update_table(self, entry):
        self.settings.style = entry["Style"].get()
        self.settings.desktop = entry["Desktop"].get()
        self.settings.silent_mode = (True if int(entry["Silent Mode"].get()) == 1 else False)
        self.settings.server_mode = (True if int(entry["Server Mode"].get()) == 1 else False)
        self.settings.server_name = entry["Server Name"].get()
        self.settings.server_user = entry["Server User"].get()
        self.settings.server_pass = entry["Server Password"].get()
        self.settings.tally_points = entry["Tally Points"].get()
        self.settings.tally_vuln = entry["Tally Vulnerabilities"].get()
        session.commit()

    def update_score(self, entry):
        self.settings.current_points = entry["Current Points"]
        self.settings.current_vuln = entry["Current Vulnerabilities"]


class CategoryModels(base):
    __tablename__ = "Vulnerability Categories"
    id = sa.Column(sa.Integer, primary_key=True)
    name = sa.Column(sa.String(128), nullable=False, unique=True)
    description = sa.Column(sa.Text, nullable=False)

    def __init__(self, **kwargs):
        super(CategoryModels, self).__init__(**kwargs)


class Categories:
    categories = {
        "Account Management": "This section is for scoring user policies. The options that will take multiple test points can be setup by clicking the `Modify` button. Once the `Modify` button is clicked that option will automatically be enabled. Make sure the option is enabled and the points are set for the options you want scored.",
        "Local Policy": "This section is for scoring Local Security Policies. Each option has a defined range that they be testing listed in their description. Make sure the option is enabled and the points are set for the options you want scored.",
        "Program Management": "This section is for scoring program manipulation. The options that will take multiple test points can be setup by clicking the `Modify` button. Once the `Modify` button is clicked that option will automatically be enabled. Make sure the option is enabled and the points are set for the options you want scored.",
        "File Management": "This section is for scoring file manipulation. The options that will take multiple test points can be setup by clicking the `Modify` button. Once the `Modify` button is clicked that option will automatically be enabled. Make sure the option is enabled and the points are set for the options you want scored.",
        "Miscellaneous": "This section is for scoring the options that do not fit into and of the other or multiple catagories. The options that will take multiple test points can be setup by clicking the `Modify` button. Once the `Modify` button is clicked that option will automatically be enabled. Make sure the option is enabled and the points are set for the options you want scored."
    }

    def __init__(self):
        loaded_categories = []
        for cat in session.query(CategoryModels):
            loaded_categories.append(cat.name)
        for cat in self.categories:
            if cat not in loaded_categories:
                name = cat
                description = self.categories[cat]
                category = CategoryModels(name=name, description=description)
                session.add(category)
        session.commit()

    def get_categories(self):
        return session.query(CategoryModels)


class VulnerabilityTemplateModel(base):
    __tablename__ = "Vulnerability Template"
    id = sa.Column(sa.Integer, primary_key=True)
    name = sa.Column(sa.String(128), nullable=False, unique=True)
    category = sa.Column(sa.Integer, sa.ForeignKey("Vulnerability Categories.id"))
    definition = sa.Column(sa.Text, nullable=False)
    description = sa.Column(sa.Text)
    checks = sa.Column(sa.Text)

    def __init__(self, **kwargs):
        super(VulnerabilityTemplateModel, self).__init__(**kwargs)


base.metadata.create_all()


class OptionTables:
    models = {}
    checks_list = {}

    def __init__(self, vulnerability_templates=None):
        loaded_vulns_templates = []
        for vuln_templates in session.query(VulnerabilityTemplateModel):
            loaded_vulns_templates.append(vuln_templates.name)
        if vulnerability_templates != None:
            for name in vulnerability_templates:
                if name not in loaded_vulns_templates:
                    category = session.query(CategoryModels).filter_by(name=vulnerability_templates[name]["Category"]).one().id
                    definition = vulnerability_templates[name]["Definition"]
                    description = vulnerability_templates[name]["Description"] if "Description" in vulnerability_templates[name] else None
                    checks = vulnerability_templates[name]["Checks"] if "Checks" in vulnerability_templates[name] else None
                    vuln_template = VulnerabilityTemplateModel(name=name, category=category, definition=definition, description=description, checks=checks)
                    session.add(vuln_template)
        session.commit()

    def initialize_option_table(self):
        for vuln_template in session.query(VulnerabilityTemplateModel):
            name = vuln_template.name
            checks_list = vuln_template.checks.split(',') if vuln_template.checks is not None else []
            checks_dict = {}
            self.checks_list.update({name: {}})
            for checks in checks_list:
                chk = checks.split(':')
                checks_dict.update({chk[0]: chk[1]})
                self.checks_list[name].update({chk[0]: chk[0]})
            create_option_table(name, checks_dict, self.models)
        base.metadata.create_all()

        for name in self.models:
            try:
                if session.query(self.models[name]).scalar() is None:
                    vuln_base = self.models[name]()
                    session.add(vuln_base)
            except:
                pass
        session.commit()

    def get_option_template(self, vulnerability):
        return session.query(VulnerabilityTemplateModel).filter_by(name=vulnerability).one()

    def get_option_template_by_category(self, category):
        return session.query(VulnerabilityTemplateModel).filter_by(category=category)

    def get_option_table(self, vulnerability, config=True):
        vuln_dict = {}
        for vuln in session.query(self.models[vulnerability]):
            if config:
                vuln_dict.update({vuln.id: {"Enabled": IntVar(value=vuln.Enabled), "Points": IntVar(value=vuln.Points), "Checks": {}}})
                for checks in vars(vuln):
                    if not checks.startswith("_") and checks != "id" and checks != "Enabled" and checks != "Points":
                        if type(vars(vuln)[checks]) == int or type(vars(vuln)[checks]) == bool:
                            vuln_dict[vuln.id]["Checks"].update({checks: IntVar(value=vars(vuln)[checks])})
                        else:
                            vuln_dict[vuln.id]["Checks"].update({checks: StringVar(value=vars(vuln)[checks])})
            else:
                vuln_dict.update({vuln.id: {"Enabled": vuln.Enabled, "Points": vuln.Points}})
                for checks in vars(vuln):
                    if not checks.startswith("_") and checks != "id" and checks != "Enabled" and checks != "Points":
                        vuln_dict[vuln.id].update({checks: vars(vuln)[checks]})
        return vuln_dict

    def add_to_table(self, vulnerability, **kwargs):
        vuln = self.models[vulnerability](**kwargs)
        session.add(vuln)
        session.commit()
        return vuln

    def update_table(self, vulnerability, entry):
        for vuln in session.query(self.models[vulnerability]):
            vuln_update = {"Enabled": (True if int(entry[vuln.id]["Enabled"].get()) == 1 else False), "Points": entry[vuln.id]["Points"].get()}
            for checks in vars(vuln):
                if not checks.startswith("_") and checks != "id" and checks != "Enabled" and checks != "Points":
                    vuln_update.update({checks: entry[vuln.id]["Checks"][checks].get()})
            session.query(self.models[vulnerability]).filter_by(id=vuln.id).update(vuln_update)
            session.commit()

    def remove_from_table(self, vulnerability, vuln_id):
        vuln = session.query(self.models[vulnerability]).filter_by(id=vuln_id).one()
        session.delete(vuln)
        session.commit()


def create_option_table(name, option_categories, option_models):
    attr_dict = {'__tablename__': name,
                 'id': sa.Column(sa.Integer, primary_key=True),
                 'Enabled': sa.Column(sa.Boolean, nullable=False, default=False),
                 'Points': sa.Column(sa.Integer, nullable=False, default=0)}
    for cat in option_categories:
        if option_categories[cat] == "Int":
            attr_dict.update({cat: sa.Column(sa.Integer, default=0)})
        elif option_categories[cat] == "Str":
            attr_dict.update({cat: sa.Column(sa.Text, default="")})

    option_models.update({name: type(name, (base,), attr_dict)})


'''
temp = Vulnerabilities.add_to_table('Remove User', **{"enabled": True, "points": 10, "User Name": 'Shaun'})
Vulnerabilities.remove_from_table('Remove User', 3)
'''
'''vulnerability_settings.update({"Main Menu": {"Style": StringVar(), "Desktop Checkbox": IntVar(), "Desktop Entry": StringVar(), "Silent Mode": IntVar(), "Server Mode": IntVar(), "Server Name": StringVar(), "Server User Name": StringVar(), "Server Password": StringVar(), "Tally Points": StringVar()},
                                       "Forensic": {"Enabled": IntVar(), "Categories": {"Points": [IntVar()], "Question": [StringVar()], "Answer": [StringVar()]}, "Location": ['']},
                                       "Account Management": {"Disable Guest": {"Definition": 'Enable this to score the competitor for disabling the Guest account in lightdm.', "Enabled": IntVar(), "Categories": {'Points': [IntVar()]}},
                                                              "Critical Users": {"Definition": 'Enable this to penalize the competitor for removing a user.',
                                                                                "Modify Definition": 'This will penalize the competitor for removing a user. To add more users press the "Add" button. To remove a user press the "X" button next to the user you want to remove. Keep it one user per line. Do not make the point value negative.',
                                                                                "Enabled": IntVar(),
                                                                                "Categories": {'Points': [IntVar()], 'User Name': [StringVar()]}},
                                                              "Add Admin": {"Definition": 'Enable this to score the competitor for elevating a user to an Administrator.',
                                                                            "Modify Definition": 'This will score the competitor for elevating a user to an Administrator. To add more users press the "Add" button. To remove a user press the "X" button next to the user you want to remove. Keep it one user per line.',
                                                                            "Enabled": IntVar(),
                                                                            "Categories": {'Points': [IntVar()], 'User Name': [StringVar()]}},
                                                              "Remove Admin": {"Definition": 'Enable this to score the competitor for demoting a user to Standard user.',
                                                                               "Modify Definition": 'This will score the competitor for demoting a user to Standard user. To add more users press the "Add" button. To remove a user press the "X" button next to the user you want to remove. Keep it one user per line.',
                                                                               "Enabled": IntVar(),
                                                                               "Categories": {'Points': [IntVar()], 'User Name': [StringVar()]}},
                                                              "Add User": {"Definition": 'Enable this to score the competitor for adding a user.',
                                                                           "Modify Definition": 'This will score the competitor for adding a user. To add more users press the "Add" button. To remove a user press the "X" button next to the user you want to remove. Keep it one user per line.',
                                                                           "Enabled": IntVar(),
                                                                           "Categories": {'Points': [IntVar()], 'User Name': [StringVar()]}},
                                                              "Remove User": {"Definition": 'Enable this to score the competitor for removing a user.',
                                                                              "Modify Definition": 'This will score the competitor for removing a user. To add more users press the "Add" button. To remove a user press the "X" button next to the user you want to remove. Keep it one user per line.',
                                                                              "Enabled": IntVar(),
                                                                              "Categories": {'Points': [IntVar()], 'User Name': [StringVar()]}},
                                                              "User Change Password": {"Definition": 'Enable this to score the competitor for changing a users password.',
                                                                                       "Modify Definition": 'This will score the competitor for changing a users password. To add more users press the "Add" button. To remove a user press the "X" button next to the user you want to remove. Keep it one user per line.',
                                                                                       "Enabled": IntVar(),
                                                                                       "Categories": {'Points': [IntVar()], 'User Name': [StringVar()]}},
                                                              "Add User to Group": {"Definition": 'Enable this to score the competitor for adding a user to a group other than the Administrative group.',
                                                                                    "Modify Definition": 'This will score the competitor for adding a user to a group other than the Administrative group. To add more users press the "Add" button. To remove a user press the "X" button next to the user you want to remove. Keep it one user  and group per line.',
                                                                                    "Enabled": IntVar(),
                                                                                    "Categories": {'Points': [IntVar()], 'User Name': [StringVar()], 'Group Name': [StringVar()]}},
                                                              "Remove User from Group": {"Definition": 'Enable this to score the competitor for removing a user from a group other than the Administrative group.',
                                                                                         "Modify Definition": 'This will score the competitor for removing a user from a group other than the Administrative group. To add more users press the "Add" button. To remove a user press the "X" button next to the user you want to remove. Keep it one user and group per line.',
                                                                                         "Enabled": IntVar(),
                                                                                         "Categories": {'Points': [IntVar()], 'User Name': [StringVar()], 'Group Name': [StringVar()]}},
                                                              "Secure Sudoers": {"Definition": 'Enable this to score the competitor for removing user names from the /etc/sudoers file.',
                                                                                 "Modify Definition": 'Enable this to score the competitor for removing user names from the /etc/sudoers file. To add more users press the "Add" button. To remove a user press the "X" button next to the user you want to remove.',
                                                                                 "Enabled": IntVar(),
                                                                                 "Categories": {'Points': [IntVar()], 'User Name': [StringVar()]}}},
                                       "Policy Options": {"Disable Auto Login": {"Definition": 'Remove all users from auto login. (lightdm)',
                                                                                 "Enabled": IntVar(),
                                                                                 "Categories": {'Points': [IntVar()]}},
                                                          "Disable User Greeter": {"Definition": 'Disable the user greeter in lightdm.',
                                                                                   "Enabled": IntVar(),
                                                                                   "Categories": {'Points': [IntVar()]}},
                                                          "Turn On Firewall": {"Definition": 'Enable this to score the competitor for turning on the firewall. (ufw)',
                                                                               "Enabled": IntVar(),
                                                                               "Categories": {'Points': [IntVar()]}},
                                                          "Firewall Rules": {"Definition": 'List of rules that need to be added or removed.',
                                                                             "Modify Definition": 'This will score the competitor for adding, removing, or modifying the desired rule in the firewall. To add more rules press the "Add" button. To remove a rule press the "X" button next to the rule you want to remove. Keep it one port per line and the ports must be numbers. The traffic is the application or protocol that will be allowed through the designated port. Uses ufw rules.',
                                                                             "Enabled": IntVar(),
                                                                             "Categories": {'Points': [IntVar()], 'Modification': [StringVar()], 'Firewall Rule': [StringVar()], 'Port Number': [StringVar()], 'Traffic': [StringVar()]}},
                                                          "Monitor Ports": {"Definition": 'List of ports that need to be open or closed.',
                                                                            "Modify Definition": 'This will score the competitor for changing the desired port to the desired state. To add more ports press the "Add" button. To remove a port press the "X" button next to the port you want to remove. Keep it one port per line and the ports must be numbers.',
                                                                            "Enabled": IntVar(),
                                                                            "Categories": {'Points': [IntVar()], 'Local Address': [StringVar()], 'Port Number': [StringVar()], 'Port State': [StringVar()]}}},
                                       "Password Policy": {"Minimum Password Age": {"Definition": 'Enable this to score the competitor for setting the minimum password age to 30, 45, or 60. (login.defs)',
                                                                                    "Enabled": IntVar(),
                                                                                    "Categories": {'Points': [IntVar()]}},
                                                           "Maximum Password Age": {"Definition": 'Enable this to score the competitor for setting the maximum password age to 60, 75, or 90. (login.defs)',
                                                                                    "Enabled": IntVar(),
                                                                                    "Categories": {'Points': [IntVar()]}},
                                                           "Minimum Password Length": {"Definition": 'Enable this to score the competitor for setting the minimum password length between 10 and 20. (pam.d/common-password)',
                                                                                       "Enabled": IntVar(),
                                                                                       "Categories": {'Points': [IntVar()]}},
                                                           "Maximum Login Tries": {"Definition": 'Enable this to score the competitor for setting the maximum login tries between 5 and 10. (login.defs)',
                                                                                   "Enabled": IntVar(),
                                                                                   "Categories": {'Points': [IntVar()]}},
                                                           "Password History": {"Definition": 'Enable this to score the competitor for setting the password history between 5 and 10. (pam.d/common-password)',
                                                                                "Enabled": IntVar(),
                                                                                "Categories": {'Points': [IntVar()]}},
                                                           "Password Complexity": {"Definition": 'Enable this to score the competitor for enabling password complexity. (pam.d/common-password)',
                                                                                   "Enabled": IntVar(),
                                                                                   "Categories": {'Points': [IntVar()]}}},
                                       "Program Management": {"Critical Programs": {"Definition": 'Enable this to penalize the competitor for removing a program.',
                                                                                   "Modify Definition": 'This will penalize the competitor for removing a program. To add more programs press the "Add" button. To remove a program press the "X" button next to the program you want to remove. Keep it one program per line.',
                                                                                   "Enabled": IntVar(),
                                                                                   "Categories": {'Points': [IntVar()], 'Program Name': [StringVar()]}},
                                                              "Good Program": {"Definition": 'Enable this to score the competitor for installing a program.',
                                                                               "Modify Definition": 'This will score the competitor for installing a program. To add more programs press the "Add" button. To remove a program press the "X" button next to the program you want to remove. Keep it one program per line.',
                                                                               "Enabled": IntVar(),
                                                                               "Categories": {'Points': [IntVar()], 'Program Name': [StringVar()]}},
                                                              "Bad Program": {"Definition": 'Enable this to score the competitor for uninstalling a program.', "Modify Definition": 'This will score the competitor for uninstalling a program. To add more programs press the "Add" button. To remove a program press the "X" button next to the program you want to remove. Keep it one program per line.',
                                                                              "Enabled": IntVar(),
                                                                              "Categories": {'Points': [IntVar()], 'Program Name': [StringVar()]}},
                                                              "Update Program": {"Definition": '(WIP)Enable this to score the competitor for updating a program.',
                                                                                 "Modify Definition": '(WIP)This will score the competitor for updating a program. To add more programs press the "Add" button. To remove a program press the "X" button next to the program you want to remove. Keep it one program per line.',
                                                                                 "Enabled": IntVar(),
                                                                                 "Categories": {'Points': [IntVar()], 'Program Name': [StringVar()]}},
                                                              "Critical Services": {"Definition": 'Enable this to penalize the competitor for modifying a services run ability.',
                                                                                    "Modify Definition": 'This will penalize the competitor for modifying a services run ability. To add more services press the "Add" button. To remove a service press the "X" button next to the service you want to remove. Keep it one service per line.',
                                                                                    "Enabled": IntVar(),
                                                                                    "Categories": {'Points': [IntVar()], 'Service Name': [StringVar()], 'Service Status': [StringVar()]}},
                                                              "Services": {"Definition": 'Enable this to score the competitor for modifying a services run ability.',
                                                                           "Modify Definition": 'This will score the competitor for modifying a services run ability. To add more services press the "Add" button. To remove a service press the "X" button next to the service you want to remove. Keep it one service per line.',
                                                                           "Enabled": IntVar(),
                                                                           "Categories": {'Points': [IntVar()], 'Service Name': [StringVar()], 'Service Status': [StringVar()]}}},
                                       "File Management": {"Bad File": {"Definition": 'Enable this to score the competitor for deleting a file.',
                                                                        "Modify Definition": 'This will score the competitor for deleting a file. To add more files press the "Add" button. To remove a file press the "X" button next to the file you want to remove. Keep it one file per line.',
                                                                        "Enabled": IntVar(),
                                                                        "Categories": {'Points': [IntVar()], 'File Path': [StringVar()]}},
                                                           "Check Hosts": {"Definition": 'Enable this to score the competitor for clearing the hosts file.',
                                                                           "Modify Definition": 'This will score the competitor for clearing the hosts file. To add more items to remove press the "Add" button. To remove an item press the "X" button next to the file you want to remove. Keep it one item per line.',
                                                                           "Enabled": IntVar(),
                                                                           "Categories": {'Points': [IntVar()], 'Text': [StringVar()]}},
                                                           "Add Text to File": {"Definition": 'Enable this to score the competitor for adding text to a file.',
                                                                                "Modify Definition": 'This will score the competitor for adding text to a file. To add more files press the "Add" button. To remove a file press the "X" button next to the file you want to remove. Keep it one file per line.',
                                                                                "Enabled": IntVar(),
                                                                                "Categories": {'Points': [IntVar()], 'Text to Add': [StringVar()], 'File Path': [StringVar()]}},
                                                           "Remove Text From File": {"Definition": 'Enable this to score the competitor for removing text from a file.',
                                                                                     "Modify Definition": 'This will score the competitor for removing text from a file. To add more files press the "Add" button. To remove a file press the "X" button next to the file you want to remove. Keep it one file per line.',
                                                                                     "Enabled": IntVar(),
                                                                                     "Categories": {'Points': [IntVar()], 'Text to Remove': [StringVar()], 'File Path': [StringVar()]}},
                                                           "File Permissions": {"Definition": 'Enable this to score the competitor for changing the permissions a user has on a file.',
                                                                                "Modify Definition": 'This will score the competitor for changing the permissions on a file based on category. To add more files press the "Add" button. To remove a file press the "X" button next to the file you want to remove. Keep it one file per line. The current file permissions will automatically be set when the file is selected. If you want to score based on different settings than change them accordingly after file selection.',
                                                                                "Enabled": IntVar(),
                                                                                "Categories": {'Points': [IntVar()], 'Permissions': [[IntVar(), IntVar(), IntVar(), IntVar(), IntVar(), IntVar(), IntVar(), IntVar(), IntVar()]], 'File Path': [StringVar()]}}},
                                       "Miscellaneous": {"Anti-Virus": {"Definition": 'Enable this to score the competitor for installing.',
                                                                        "Enabled": IntVar(),
                                                                        "Categories": {'Points': [IntVar()]}},
                                                         "Update Check Period": {"Definition": 'Enable this to score the competitor for setting the period the system checks for updates to once a week.',
                                                                                 "Enabled": IntVar(),
                                                                                 "Categories": {'Points': [IntVar()]}},
                                                         "Update Auto Install": {"Definition": 'Enable this to score the competitor for setting the system updates to automatically install updates.',
                                                                                 "Enabled": IntVar(),
                                                                                 "Categories": {'Points': [IntVar()]}},
                                                         "Update Kernel": {"Definition": '(WIP)Update the kernel to the newest version.',
                                                                           "Enabled": IntVar(),
                                                                           "Categories": {'Points': [IntVar()]}},
                                                         "Cron Tab": {"Definition": 'This will score the competitor for removing a task from the cron tab.',
                                                                      "Modify Definition": 'This will score the competitor for removing a task from the cron tab. To add more tasks press the "Add" button. To remove a task press the "X" button next to the task you want to remove. Keep it one task per line.',
                                                                      "Enabled": IntVar(),
                                                                      "Categories": {'Points': [IntVar()], 'User Name': [StringVar()], 'Task Name': [StringVar()]}},
                                                         "Check Startup": {"Definition": 'Enable this to score the competitor for removing or disabling a program from the startup.',
                                                                           "Modify Definition": 'This will score the competitor for removing or disabling a program from the startup. To add more programs press the "Add" button. To remove a program press the "X" button next to the program you want to remove. Keep it one program per line and include the entire name from the /etc/rc?.d directory. ? is a single character wildcard.',
                                                                           "Enabled": IntVar(),
                                                                           "Categories": {'Points': [IntVar()], 'Program Name': [StringVar()]}}}})'''
